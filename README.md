## Mobilizing.js
Mobilizing.js is a software authoring environment for artists and designers intended to encourage the creation of interactive works on various forms of device-screens. The great versatility of the increasingly well-established JavaScript makes it possible for Mobizling.js to expand its field of action to many software environments, including browsers, Node.js servers, or even the contexts specific to certain machines (mobile devices—tablets, smartphones—iOS, Android, Windows, etc.). The foundations of Mobilizing.js take shape as a JavaScript library that specifies a programming interface developed for interactive artistic creation in hardware and software environments that are now unified by the JavaScript language.

It is developped at EnsadLab, the research laboratory of Ecole nationale supérieure des Arts Décoratifs de Paris, Reflective Interaction research Program, by Dominique Cunin, Jonathan Tanant, Oussama Mubarak.

## Usage

First install the dependency on your project.

```sh
npm install --save-dev '@mobilizing/library'
```

Then use it in your code.

```javascript
import * as Mobilizing from '@mobilizing/library';

const context = new Mobilizing.Context();
const script = new UserScript(); // this is yours
context.addComponent(script);
const runner = new Mobilizing.Runner( {context} );
```
