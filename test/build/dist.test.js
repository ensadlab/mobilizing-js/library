import * as library from '../../dist/Mobilizing.js';
import { runTests } from '../shared/base.js';

describe('base functionality from distribution', () => {
    runTests(library);
});
