import { runTests } from '../shared/base.js';
import { debug, appendScript } from '../shared/utils';

describe('base functionality from build', () => {
  const library = appendScript({
    url: 'build/mobilizing.bundle.js',
  }).then( () => {
    return self.Mobilizing;
  }).catch( (error) => {
    debug('error', error);
    return new Error(error);
  });

  runTests(library);
});
