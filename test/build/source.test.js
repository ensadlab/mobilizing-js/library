import * as library from '../../src/js/Mobilizing.js';
import { runTests } from '../shared/base.js';

describe('base functionality from sources', () => {
    runTests(library);
});
