import { Context, Component } from '../../../src/js/Mobilizing.js';

describe('core/Component', () => {
  test('default constructor', () => {
    let component = null;
    expect( () => {
      component = new Component();
    }).not.toThrow();

    component.setup();
    expect(component._setupDone).toBe(true);

    expect(component.active).toBe(false);
    component.on();
    expect(component.active).toBe(true);
    component.off();
    expect(component.active).toBe(false);
  });

  test('constructor with name, id, and context', () => {
    const context = new Context();
    const id = 123;
    let component = null;

    expect( () => {
      const name = 'my component';
      component = new Component({context, id, name});
      component.setup();
    }).not.toThrow();
  });

  test('chained components', () => {
    const rootComponent = new Component();

    const chainedComponents = [];
    for(let i = 0; i < 10; i++) {
      const component = new Component({name: `component ${i}`})
      chainedComponents.push(component);

      rootComponent.chain(component);
    }

    expect(rootComponent._setupDone).toBe(false);
    chainedComponents.forEach( (component) => {
      expect(component._setupDone).toBe(false);
    });

    // setup of root component must setup chained components
    rootComponent.setup();
    expect(rootComponent._setupDone).toBe(true);
    chainedComponents.forEach( (component) => {
      expect(component._setupDone).toBe(true);
    });
  });

});
