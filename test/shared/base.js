const e = {};

import { debug, assetsUrl, getFlagFromEnv } from './utils.js';

export class UserScript {
    constructor() {
        debug('constructor');
    }

    preLoad(loader) {
        const promises = [];

        promises.push(new Promise( (resolve, reject) => {
            const url = `${assetsUrl}/fonts/Raleway-Regular.ttf`;
            const onLoad = () => {
                debug(`fontFile ${url} loaded`);
                resolve();
            };
            const onError = (error) => {
                reject(error);
            };
            this.fontFile = loader.loadArrayBuffer({
                url,
                onLoad,
                onError,
            });
        }));

        promises.push(new Promise( (resolve, reject) => {
            const url = `${assetsUrl}/guitar1.wav`;
            const onLoad = () => {
                debug(`fontFile ${url} loaded`);
                resolve();
            };
            const onError = (error) => {
                reject(error);
            };
            this.audioBufferRequest = loader.loadArrayBuffer({
                url,
                onLoad,
                onError,
            });
        }));

        this.ready = Promise.all(promises);
    }

    setup() {
        debug('setup');
    }

    addNewLetter(event) {
        // See https://developer.mozilla.org/fr/docs/Web/API/KeyboardEvent/key/Key_Values
        if (event.key.length === 1) {
            debug("addLetter",event.key);
            this.textField.addLetter(event.key);
        }
        else {
            debug(event);
            switch (event.key) {
              case 'ArrowLeft':
                  this.textField.moveCursorBack();
                  break;

              case 'ArrowRight':
                  this.textField.moveCursorForward();
                  break;

              case 'Enter':
                  debug("isEnter");
                  this.textField.addLetter("\n");
                  break;

              case 'Backspace':
                  debug("isDelete");
                  this.textField.delete();
                  break;

              default:
                  console.error('unexpected event.key for addLetter', event.key);
                  break;
            }

        }
    }

    cameraCallback(userMedia){

        this.cameraUserMedia = userMedia;
        this.canvas = userMedia.canvas;
        document.body.appendChild(this.canvas);

        this.cameraIsReady = true;

        debug("cameraCallback",this);
    }

    update() {
        if(this.currentTest === "Motion") {
            debug(this.motion.acc);
        }

        if(this.currentTest === "Camera") {
            if(this.cameraIsReady) {
                const drawContext = this.canvas.getContext("2d");
                drawContext.drawImage(this.cameraUserMedia.videoEl, 0,0);

            }
        }

        if(this.currentTest === "Gamepad") {
            debug(this.gamepad.getButton(0));
            debug(this.gamepad.getAxes(0), this.gamepad.getAxes(1));
        }
    }

}
Object.assign(e, { UserScript } );

export function runTests(library) {
    /* eslint-disable */
    let Mobilizing;
    let context;
    let script;
    let runner;

    /* eslint-enable */

    const setup = (lib) => {
        Mobilizing = lib;
        context = new Mobilizing.Context();
        script = new UserScript();
        context.addComponent(script);
        runner = new Mobilizing.Runner( {context} );
    }

    // new context for each test
    beforeEach( () => {
        if (library instanceof Promise) {
            return library.then(setup);
        }

        return setup(library);
    });

    test('Font', (done) => {
        script.ready.then( () => {
            const fontBuffer = script.fontFile.getValue();
            expect(typeof fontBuffer).toBe('object');
            expect(fontBuffer.byteLength).toBe(176188);

            script.font = new Mobilizing.Font( {fontFile: fontBuffer} );
            expect(script.font.font.names.fontFamily.en).toBe('Raleway');

            let textSize = script.font.getTextSize("TRUC", 20)
            expect(textSize.width).toBeCloseTo(54.08);
            expect(textSize.height).toBeCloseTo(14.4);

            script.font64 = new Mobilizing.Font( {base64String: Mobilizing.Raleway.regular} );
            expect(script.font64.font.names.fontFamily.en).toBe('Raleway');

            textSize = script.font64.getTextSize("TRUC", 20);
            expect(textSize.width).toBeCloseTo(54.08);
            expect(textSize.height).toBeCloseTo(14.4);

            expect( () => {
                const canvas = document.createElement("canvas");
                canvas.width = textSize.width;
                canvas.height = textSize.height;

                script.font64.getFont().draw(canvas.getContext("2d"),
                                             "TRUC", 0, textSize.height, 20);
                document.body.appendChild(canvas);
            }).not.toThrow();

            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Styled letter', (done) => {
        script.ready.then( () => {
            const fontBuffer = script.fontFile.getValue();
            script.font = new Mobilizing.Font( {fontFile: fontBuffer} );
            const styledLetter = new Mobilizing.StyledLetter({
                "letter": "W",
                "font":  script.font,
            });
            expect(styledLetter.width).toBeCloseTo(41.84);
            expect(styledLetter.height).toBeCloseTo(28.4);
            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Styled text element', (done) => {
        script.ready.then( () => {
            const fontBuffer = script.fontFile.getValue();
            script.font = new Mobilizing.Font( {fontFile: fontBuffer} );
            //@TODO passer en symétrie namespace avec la structure du repo.
            //script.font = new Mobilizing.text.Font( {fontFile: fontBuffer} );

            const styledText = new Mobilizing.StyledTextElement({
                text: 'Styled text!',
                font:  script.font,
            });
            expect(styledText.width).toBeCloseTo(207.8);
            expect(styledText.height).toBeCloseTo(29.76);
            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Rich text', (done) => {
        script.ready.then( () => {

            //beware of the very few html tags we support : b || strong, i || em, p, font with attributes color && size (face TODO).
            const text = "Ceci est <b><font face='Avenir' color='blue' size='10'>un text</font></b> qui <i>utilise</i> des <i><b>balises</b> HTML.</i><br>Mais <i>est-ce que <b>ça marche</b> bien <b>ou <strong>encore</strong></b> pas?</i><br>On dirait bien que <b><font color='red' size='22'>SIIII!</font></b>\nPour le moment, on utilise des fonts chargées par defaut, mais on peut passer en argument des fonts <b><font size='40' color='green'>preloadées</font></b>.\nNe pas faire n'importe quoi avec les balises reste de la <i><b>résponsabilité</i></b> de l'utilisateur. <p>Non, mais, faudrait quand même pas trop déconner non plus, <b><font size='30'>HEEEEIIIIIIN!</font></b></p><p><b><i>Normal quoi,</i> non ?</b></p>";

            //make a richText representation of html
            const richText = new Mobilizing.RichText({
                width: 512,
                height: 512,
                text,
                lineHeight: 1.5,
                margin:50,
                backgroundColor: "darkgray",
                textColor: "white",
                textAlign: "right"
            });

            const canvas = richText.getCanvas();

            expect( () => document.body.appendChild(canvas) ).not.toThrow();

            const textDimensions = richText.getTextDimensions();
            expect(textDimensions.width).toBeCloseTo(502);
            expect(textDimensions.height).toBeCloseTo(344.9);

            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Text field', (done) => {
        script.ready.then( () => {
            script.textField = new Mobilizing.TextField();
            document.body.appendChild(script.textField.getCanvas());

            const keyboard = new Mobilizing.input.Keyboard({target: window});
            keyboard.setup();
            keyboard.on();
            keyboard.events.on("keydown", script.addNewLetter.bind(script));

            [
                'a',          // a|
                'b',          // ab|
                'c',          // abc|
                'ArrowLeft',  // ab|c
                'ArrowLeft',  // a|bc
                'Backspace',  // |bc
                'ArrowRight', // b|c
                'e',          // be|c
                'Enter',      // be
                              // |c
                'f',          // be
                              // f|c
            ].forEach( (eventKey) => {
                const event = new self.Event('keydown');
                event.key = eventKey;
                self.dispatchEvent(event);
            });

            const letterPicked = script.textField.pickLetter(30, 26);
            expect(letterPicked).toBeDefined();
            expect(letterPicked.index).toBe(1);
            expect(script.textField._cursorIndex).toBe(3);

            // go after last letter
            expect(() => script.textField.moveCursorTo(4) ).not.toThrow();
            expect(script.textField._cursorIndex).toBe(4);

            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Motion', (done) => {
        script.ready.then( () => {
            script.motion = new Mobilizing.input.Motion();
            script.motion.setup();
            script.motion.on();

            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Mouse', (done) => {
        script.ready.then( () => {
            script.mouse = new Mobilizing.input.Mouse();
            script.mouse.setup();
            script.mouse.on();

            script.mouse.events.on('mousemove', (event) => {
                expect(event).toBeDefined();
                done();
            });

            const event = new self.MouseEvent('mousemove');
            self.dispatchEvent(event);
        }).catch( (error) => {
            done(error);
        });
    });

    test('Touch', (done) => {
        script.ready.then( () => {
            script.touch = new Mobilizing.input.Touch();
            script.touch.setup();
            script.touch.on();

            const promises = [];
            [
                'touchstart',
                'touchmoved', // warning, this is not 'touchmove'
                'touchend',
            ].forEach( (eventName) => {
                promises.push(new Promise( (resolve) => {
                    script.touch.events.on(eventName, (event) => {
                        expect(event).toBeDefined();
                        resolve();
                    });
                }));
            });

            const touch = new Touch({
                identifier: 1,
                target: self,
            });

            [
                'touchstart',
                'touchmove',
                'touchend',
            ].forEach( (eventName) => {
                const event = new self.TouchEvent(eventName, {
                    touches: [ touch ],
                    targetTouches: [ touch ],
                    changedTouches: [ touch ],
                });
                self.dispatchEvent(event);
            });

            Promise.all(promises).then(() => done()).catch(done);

        }).catch( (error) => {
            done(error);
        });
    });

    test('Pointer', (done) => {
        script.ready.then(() => {
            //inputs
            script.touch = script.context.addComponent(new Mobilizing.input.Touch({ target: window}));
            script.touch.setup();//set it up
            script.touch.on();//active it

            script.mouse = script.context.addComponent(new Mobilizing.input.Mouse({ target: window}));
            script.mouse.setup();//set it up
            script.mouse.on();//active it

            script.pointer = new Mobilizing.input.Pointer();
            script.context.addComponent(script.pointer);
            script.pointer.add(script.touch);
            script.pointer.add(script.mouse);

            expect(script.pointer.pointers.size).toBe(2);

            // for mouse and touch
            const promises = [];

            // mouse
            ['mousemove'].forEach( (eventName) => {
                promises.push(new Promise( (resolve) => {
                    script.mouse.events.on(eventName, (event) => {
                        expect(event).toBeDefined();
                        resolve();
                    });
                }));

                const event = new self.MouseEvent(eventName);
                self.dispatchEvent(event);
            });

            // touch
            [
                'touchstart',
                'touchmoved', // warning, this is not 'touchmove'
                'touchend',
            ].forEach( (eventName) => {
                promises.push(new Promise( (resolve) => {
                    script.touch.events.on(eventName, (event) => {
                        expect(event).toBeDefined();
                        resolve();
                    });
                }));
            });

            const touch = new Touch({
                identifier: 2,
                target: self,
            });

            [
                'touchstart',
                'touchmove',
                'touchend',
            ].forEach( (eventName) => {
                const event = new self.TouchEvent(eventName, {
                    touches: [ touch ],
                    targetTouches: [ touch ],
                    changedTouches: [ touch ],
                });
                self.dispatchEvent(event);
            });

            Promise.all(promises).then(() => done()).catch(done);

        }).catch( (error) => {
            done(error);
        });
    });

    const NO_CAMERA = getFlagFromEnv('NO_CAMERA');
    if(NO_CAMERA) {
        test.skip('Camera', () => {});
    }
    /* else {
        test('Camera', (done) => {
            script.ready.then( () => {
                const constraints = {
                    audio: false,
                    video: {facingMode: "user"}
                };
                script.camera = new Mobilizing.input.UserMedia({
                    constraints,
                    callback: () => done(),
                });

                script.context.addComponent(script.camera);
                script.camera.setup();

                expect( () => script.camera.on() ).not.toThrow();
                expect( () => script.camera.off() ).not.toThrow();

            }).catch( (error) => {
                done(error);
            });
        });
    } */

    test('Gamepad', (done) => {
        script.ready.then( () => {
            script.gamepad = new Mobilizing.input.Gamepad();
            script.context.addComponent(script.gamepad);
            script.gamepad.setup();
            expect( () => script.gamepad.on() ).not.toThrow();
            expect( () => script.gamepad.off() ).not.toThrow();

            done();
        }).catch( (error) => {
            done(error);
        });
    });

    test('Timer', (done) => {
        script.ready.then( () => {
            script.timer = new Mobilizing.Timer({
                interval: 100,
                callback: () => {
                    debug('timer');
                    done();
                },
            });

            script.context.addComponent(script.timer);
            script.timer.setup();
            script.timer.on();
            script.timer.start();

        }).catch( (error) => {
            done(error);
        });
    });

    test('Audio', (done) => {
        script.ready.then( () => {

            script.mouse = new Mobilizing.input.Mouse({target: window});
            script.mouse.setup();
            script.mouse.on();

            if(!script.audioRenderer) {
                script.audioRenderer = new Mobilizing.audio.Renderer();
                script.context.addComponent(script.audioRenderer);

                script.audioRenderer.on();
                debug("beep");
                expect( () => script.audioRenderer.beep() ).not.toThrow();

                const source = new Mobilizing.audio.Source({renderer : script.audioRenderer});
                source.setLoop(true);

                const sound = new Mobilizing.audio.Buffer({
                    renderer: script.audioRenderer,
                    arrayBuffer: script.audioBufferRequest.getValue(),
                    decodedCallback: () => {
                        debug("play!");
                        source.setBuffer(sound);
                        expect(() => source.play() ).not.toThrow();
                        done();
                    }
                });
            }

        }).catch( (error) => {
            done(error);
        });
    });

    test('Three renderer', (done) => {
        script.ready.then( () => {

            script.renderer = new Mobilizing.three.RendererThree();
            script.context.addComponent(script.renderer);

            script.camera = new Mobilizing.three.Camera();
            script.renderer.addCamera(script.camera);

            script.renderer.setClearColor(Mobilizing.three.Color.darkGray.clone());

            const light = new Mobilizing.three.Light();
            expect(light.color).toMatchObject({r: 1, g: 1, b: 1});
            expect(light.distance).toBeCloseTo(5000);

            script.renderer.addToCurrentScene(light);

            const box1 = new Mobilizing.three.Box();
            box1.transform.setLocalPosition(0,0,-10);
            box1.transform.setLocalRotation(33,20,-33);

            const transform = box1.transform.nativeObject;

            expect(transform.up.x).toBeCloseTo(0);
            expect(transform.up.y).toBeCloseTo(1);
            expect(transform.up.z).toBeCloseTo(0);

            expect(transform.position.x).toBeCloseTo(0);
            expect(transform.position.y).toBeCloseTo(0);
            expect(transform.position.z).toBeCloseTo(-10);

            expect(transform.quaternion._x).toBeCloseTo(0.2208945841796256);
            expect(transform.quaternion._y).toBeCloseTo(0.23908013446497267);
            expect(transform.quaternion._z).toBeCloseTo(-0.22089458417962565);
            expect(transform.quaternion._w).toBeCloseTo(0.9193757962141657);

            expect( () => script.renderer.addToCurrentScene(box1) ).not.toThrow();

            done();
        }).catch( (error) => {
            done(error);
        });
    });

}
Object.assign(e, { runTests } );


export default e;
