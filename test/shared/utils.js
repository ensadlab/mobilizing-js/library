import electron from 'electron';
import Debug from 'debug';

const e = {};

export const debug = new Debug('mobilizing:library');
Object.assign(e, { debug });

export const baseUrl = `file://${__dirname}/../..`;
Object.assign(e, { baseUrl });

export const assetsUrl = `file://${__dirname}/../assets`;
Object.assign(e, { assetsUrl });

export function getFlagFromEnv(name) {
  return typeof process.env[name] !== 'undefined'
    && parseInt(process.env[name], 10) !== 0;
}
Object.assign(e, { getFlagFromEnv });


export function showWindowOrDebug({
  showWindow = false,
  debugTools = false,
} = {}) {

  const promise = new Promise( (resolve) => {
    const currentWindow = electron.remote.getCurrentWindow();
    if(debugTools) {
      currentWindow.on('show', () => {
        // wait a little more
        setTimeout( () => {
          debugger; // eslint-disable-line
          resolve();
        }, 1000);
      });
      currentWindow.toggleDevTools();
    }
    else {
      resolve();
    }

    if(showWindow || debugTools) {
      currentWindow.show();
    }

  });

  return promise;
}

Object.assign(e, { showWindowOrDebug });

export function appendScript({
  parent = document.body,
  url = '',
} = {}) {
  return new Promise((resolve, reject) => {
    const element = document.createElement('script');
    element.type = 'text/javascript';
    element.src = `${baseUrl}/${url}`;
    // load in parallel and execute according to order of declaration
    element.defer = true;

    window.onerror = function(event){
      reject(new Error(event));
    };

    element.onload = () => {
      debug(`${baseUrl}/${url} loaded`);
      resolve(element);
    };

    element.onerror = (event) => {
      debug(`Error while appending ${baseUrl}/${url} script`,
                    JSON.stringify(event) );
      reject(new Error(element.src));
    };

    parent.append(element);

  });
}

Object.assign(e, { appendScript });

export default e;
