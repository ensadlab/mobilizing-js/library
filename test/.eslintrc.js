module.exports = {
    extends: '../.eslintrc.js',
    env: {
        es6: true,
        browser: true,
        jest: true,
        node: true,
    },
};
