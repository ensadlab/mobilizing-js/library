module.exports = {
    tests: false, // see prepublishOnly script in package.json
    releaseDraft: false, // do not open a draft release on Github
    yarn: false, // do not publish to yarn
    testScript : "test-jest"
};
