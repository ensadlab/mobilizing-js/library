# Directories

- `src` : source files
- `test` : test files
- `dist` : where distribution files are stored after building
- `build` : where bundle files are stored after building
- `doc` : where library documentation is stored after building
- `examples` : library usage examples


# NPM Usage
If you want to install the complete developpement framework we use for writting Mobilizing.js, you need to use Node.js to install all the needed programs.

```sh
npm install
```

Once installed, you can use the scripts defined in `package.json` to help your code writting.

- `npm run build` to build everything: documentation, distribution and bundle files.
- `npm run build-doc` to build the documentation only.
- `npm run clean` to remove the built files.
- `npm run test-lint` to see if you comply with the coding practices of this library, defined in `.eslintrc.js`.
- `npm run test-watch` to compile both the library sources and the test sources whenever they change.
- `npm run dev-build-dist-watch` to compile the library to the `dist` directory whenever the source code changes.

Furthermore, you can use your own commands. You need to prefix them with `npx` to use the installed `dev-dependencies`.

- `npx babel --help` to get help about babel
- `npx babel src/js -d dist --source-maps=true --watch` for an equivalent of `npm run dev-build-dist-watch`
- `npx jest test/dist/* --watchAll` to run test on the distribution files whenever they change

# Work flow

Do any specific work in a branch. Then merge into `develop`.


## Work on the library

In a terminal, you can use the following command to run the tests, whenever the tests or the source files change.

```sh
npm run test-watch
```

If you are creating a new feature or fixing a bug, you should also create a test for that, during the development process. See `test/unit` and `test/bugfixes` directories.


## NPM link to refer from other project

To work on the library which is used by other projects locally, you can declare a global NPM link in the `library` and use it from any other project.

First, declare a global NPM link to the `library`, from its directory.

```sh
cd 'mobilizing-js/library'
npm link
```

In a terminal, you should run the tests as before. In an other terminal, you can also build the distributions files, whenever they change. The link will refer to this build, in the `dist` directory.

```sh
npm run dev-build-dist-watch
```

Then, in an other terminal for the other project, install the dependencies as usual, but use the global NPM link to the `library`.

```sh
cd 'other-project'
npm install
npm link '@mobilizing/library'
```

From now on, the distribution of `@mobilizing/library` is not from NPM any more, it is linked to your local files.

You should also watch from there, to rebuild the project on changes, run tests, etc.

### Caveat

There is a caveat. Whenever you will make a `npm install`, the link will disappear. You can simply restore it by running `npm link '@mobilizing/library'` again. (Idem `npm link` will restore the source link of the `library`.)


# Release

To make a release, merge `develop` into `master`. Then, still on `master` branch, make a version and deploy it to NPM and Git.

```sh
npm run deploy
```

This will do a number of checks, increment the version, and publish to NPM and Git.

- check that the branch is `master`
- check that the current Git status is clean
- increment the version in `package.json`, create a commit and a tag for Git with that version
- reinstall NPM dependencies
- build an test everything
- publish to NPM
- publish to Git

To preview a publication, without actually changing anything you can use

```sh
npm run deploy-preview
```

Note that the script `prepublishOnly`  in `package.json` runs each time there is a publication to NPM: it builds everything and run the tests.

# Git flow

## Branches

Create a branch for any specific work: to create a feature, to fix a bug, etc.

### Create new branch:

Make sure you don't have any uncommitted work.

```sh
git checkout develop
git checkout -b <feature|bugfix>/<name>
```

### Finish a branch:

Commit changes to the branch, merge into `develop` branch, then delete the specific branch.

```sh
git add --all # or chose what to add
git commit --all
git checkout develop
git merge --no-ff <feature|bugfix>/<name>
git branch --delete <feature|bugfix>/<name>
```

## Commit message convention

### Format

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

### `<type>`
One of the following allowed types:
* __feat__: when new functionalities are added
* __fix__: when bugs are fixed
* __docs__: when documentation is added/changed
* __style__: when code style (formatting, missing semi colons, …) is modified
* __refactor__: when a refactoring is done
* __test__: when tests are added
* __chore__: when a maintenance task is done with no production code change (updating an npm script, etc)


### `<scope>`
Anything specifying place of the commit change. For example “Panel”, “Element”, “Player”, “Editor”. It can be empty (e.g. if the change is a global or difficult to assign to a single component), in which case the parentheses are omitted.

### `<subject>`
Contains succinct description of the change.
* use imperative, present tense: “change” not “changed” nor “changes”
* don't capitalize first letter
* no dot (.) at the end

### `<body>`
Contains motivation for the change and contrasts with previous behavior.
* use imperative, present tense: “change” not “changed” nor “changes”

### `<footer>`
Contains special notes such as references to issues, breaking changes, etc.
* closed issues should be listed as a comma separated list on a separate line in the footer prefixed with "Closes" keyword
* breaking changes have to be mentioned with the description of the change, justification and migration notes

### Examples
```
fix(Editor): remove duplicate keyboard listeners

Remove keyboard event listeners on the Player's body added from within the Editor as they are already captured on the iFrame

Closes #240, #254
Breaks foo.bar, foo.baz should be used instead
```

```
style(Player): add some missing semi colons
```
