const webpack = require('webpack');
const path = require('path');
const git = require('git-rev-sync');
const pckg = require('./package.json');
const ESLintPlugin = require('eslint-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const sourcePattern = /\.js$/;
const temporaryPattern = /(^[.#].*)|(.*~$)/;

module.exports = (env, argv) => {
    const production = argv.mode === 'production';

    return {
        bail: true,
        entry: ['./src/js/Mobilizing.js'],
        output: {
            filename: production ? 'mobilizing.bundle.min.js' : 'mobilizing.bundle.js',
            library: 'Mobilizing',
            path: path.join(__dirname, "build"),
            libraryTarget: 'umd',
            umdNamedDefine: true, // be careful: library name is global
            // @TODO: define default, is is really good practice
            // libraryExport: 'default', // assign defaults to target
            globalObject: 'this',
        },
        optimization: {
            minimize: production,
            minimizer: [
                new TerserPlugin({ parallel: true })
            ],
        },
        devtool: production ? false : 'source-map',
        module: {
            rules: [
                {
                    test: (modulePath) => {
                        return sourcePattern.test(modulePath)
                            && !temporaryPattern.test(path.basename(modulePath));
                    },
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env'],
                            },
                        },
                        {
                            loader: 'string-replace-loader',
                            options: {
                                multiple: [
                                    {
                                        search: '<@VERSION@>',
                                        replace: pckg.version,
                                    },
                                    {
                                        search: '<@REVISION@>',
                                        replace: git.short(),
                                    },
                                ]
                            },
                        },
                    ],
                    exclude: '/node_modules/',
                }, //
            ], // rules
        }, // module
        plugins: [
            new webpack.BannerPlugin(`${pckg.name} - v${pckg.version} r${git.short()}`),
            new ESLintPlugin({
                failOnWarning: false,
                failOnError: true,
            }),
        ],
    };
}// modules exports
