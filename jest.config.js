// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {

  // An array of directory names to be searched recursively up from the requiring module's location
  moduleDirectories: [
    "node_modules",
  ],

  // An array of file extensions your modules use
  moduleFileExtensions: [
    "js",
  ],

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // See https://stackoverflow.com/a/71088205/2811145
  moduleNameMapper: {
    'three/examples/jsm/': '<rootDir>/test/mocks/mockUndefined.js'
  },

  modulePaths: [
    "<rootDir>/vendor"
  ],

  runner: '@jest-runner/electron',
  testEnvironment: '@jest-runner/electron/environment',
  // testEnvironmentOptions: {
  //   nodeIntegration: false,
  // },
  testMatch: [
    '<rootDir>/test/**/*.test.?(m)[jt]s?(x)',
  ],
};
