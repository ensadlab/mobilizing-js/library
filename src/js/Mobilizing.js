import "regenerator-runtime/runtime";
import * as THREE from 'three';
export { THREE }; //to use Three.js directly in user script but with Mobilizing full compatibility, we must use the same ESModule between the script and the library, or some things (instanceof) can go wrong.

export { default as Component } from "./Mobilizing/core/Component";
export { default as Context } from "./Mobilizing/core/Context";
export { default as Basic3DContext } from "./Mobilizing/core/context/Basic3DContext";
export { default as MultiScriptContext } from "./Mobilizing/core/context/MultiScriptContext";
export { default as Runner } from "./Mobilizing/core/Runner";

/* import * as debug from "./Mobilizing/core/util/Debug";
export { debug }; */

import * as util from "./Mobilizing/core/util/Misc";
export { util };

export { default as Device } from "./Mobilizing/core/util/Device";
export { getElementPosition } from "./Mobilizing/core/util/Dom";
export { default as EventEmitter } from "./Mobilizing/core/util/EventEmitter";
export { default as Loader } from "./Mobilizing/core/util/Loader";

import * as math from "./Mobilizing/core/util/Math";
export { math };

export { throttle, getUrlParameter, encode64, Base64Binary } from "./Mobilizing/core/util/Misc";

import Ajax from "./Mobilizing/net/Ajax";
import PubSub from "./Mobilizing/net/PubSub";
import Storage from "./Mobilizing/net/Storage";

export const net = {
    Ajax,
    PubSub,
    Storage,
};

import GPS from "./Mobilizing/input/GPS";
import GPSUtils from "./Mobilizing/input/GPSUtils";
import Keyboard from "./Mobilizing/input/Keyboard";
import Motion from "./Mobilizing/input/Motion";
import Mouse from "./Mobilizing/input/Mouse";
import Orientation from "./Mobilizing/input/Orientation";
import Touch from "./Mobilizing/input/Touch";
import Pointer from "./Mobilizing/input/Pointer";
import UserMedia from "./Mobilizing/input/UserMedia";
import Gamepad from "./Mobilizing/input/Gamepad";
import DualShock4 from "./Mobilizing/input/DualShock4";

export const input = {
    GPS,
    GPSUtils,
    Keyboard,
    Motion,
    Mouse,
    Orientation,
    Touch,
    Pointer,
    UserMedia,
    Gamepad,
    DualShock4,
}

export { default as Animation } from "./Mobilizing/misc/Animation";
export { default as BatteryStatus } from "./Mobilizing/misc/BatteryStatus";

export { default as Font } from "./Mobilizing/text/Font";
export { default as StyledLetter } from "./Mobilizing/text/StyledLetter";
export { default as StyledTextElement } from "./Mobilizing/text/StyledTextElement";
export { default as RichText } from "./Mobilizing/text/RichText";
export { default as TextField } from "./Mobilizing/text/TextField";

export { Raleway } from "./Mobilizing/misc/DefaultFonts";

export { default as Time } from "./Mobilizing/time/Time";
export { default as Timer } from "./Mobilizing/time/Timer";

import MIDI from "./Mobilizing/serial/Midi";
export const serial = {
    MIDI,
}

import Buffer from "./Mobilizing/renderer/audio/Buffer";
import Source from "./Mobilizing/renderer/audio/Source";
import Renderer from "./Mobilizing/renderer/audio/Renderer";
import OfflineRenderer from "./Mobilizing/renderer/audio/OfflineRenderer";
import BufferToWav from "./Mobilizing/renderer/audio/BufferToWav";

export const audio = {
    Buffer,
    Source,
    Renderer,
    OfflineRenderer,
    BufferToWav,
}

import { default as RendererThree } from "./Mobilizing/renderer/3D/RendererThree";

//types
import Color from "./Mobilizing/renderer/3D/three/types/Color";
import Vector2 from "./Mobilizing/renderer/3D/three/types/Vector2";
import Vector3 from "./Mobilizing/renderer/3D/three/types/Vector3";
import Line3 from "./Mobilizing/renderer/3D/three/types/Line3";
import Matrix3 from "./Mobilizing/renderer/3D/three/types/Matrix3";
import Matrix4 from "./Mobilizing/renderer/3D/three/types/Matrix4";
import Ray from "./Mobilizing/renderer/3D/three/types/Ray";
import Euler from "./Mobilizing/renderer/3D/three/types/Euler";
import Quaternion from "./Mobilizing/renderer/3D/three/types/Quaternion";
import Rect from "./Mobilizing/renderer/3D/three/types/Rect";

//scene
import Camera from "./Mobilizing/renderer/3D/three/scene/Camera";
import Light from "./Mobilizing/renderer/3D/three/scene/Light";
import Transform from "./Mobilizing/renderer/3D/three/scene/Transform";
import Material from "./Mobilizing/renderer/3D/three/scene/Material";
import CubeMapCamera from "./Mobilizing/renderer/3D/three/scene/CubeMapCamera";

//texture
import Texture from "./Mobilizing/renderer/3D/three/texture/Texture";
import VideoTexture from "./Mobilizing/renderer/3D/three/texture/VideoTexture";
import RenderTexture from "./Mobilizing/renderer/3D/three/texture/RenderTexture";
import ImageSequenceTexture from "./Mobilizing/renderer/3D/three/texture/ImageSequenceTexture";

//Meshes
import Mesh from "./Mobilizing/renderer/3D/three/shape/Mesh";
//primitives
import Box from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Box";
import Sphere from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Sphere";
import Node from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Node";
import Cube from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Cube";
import Icosahedron from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Icosahedron";
import Line from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Line";
import Segments from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Segments";
import Points from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Points";
import Octahedron from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Octahedron";
import Tetrahedron from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Tetrahedron";
import Dodecahedron from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Dodecahedron";
import Cylinder from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Cylinder";
import Sprite from "./Mobilizing/renderer/3D/three/shape/3D/primitive/Sprite";

//2D
import Arc from "./Mobilizing/renderer/3D/three/shape/2D/Arc";
import Circle from "./Mobilizing/renderer/3D/three/shape/2D/Circle";
import Quad from "./Mobilizing/renderer/3D/three/shape/2D/Quad";
import Plane from "./Mobilizing/renderer/3D/three/shape/2D/Plane";

import EdgesMesh from "./Mobilizing/renderer/3D/three/shape/3D/composite/EdgesMesh";
import Text from "./Mobilizing/renderer/3D/three/shape/3D/composite/Text";
import TextOpentype from "./Mobilizing/renderer/3D/three/shape/3D/composite/TextOpentype";
import TextTroika from "./Mobilizing/renderer/3D/three/shape/3D/composite/TextTroika";

//parsers
import MeshImport from "././Mobilizing/renderer/3D/three/shape/3D/parser/MeshImport"

//UI
import Clickable from "./Mobilizing/renderer/3D/three/ui/Clickable";
import Button from "./Mobilizing/renderer/3D/three/ui/Button";
import ButtonGroup from "./Mobilizing/renderer/3D/three/ui/ButtonGroup";
import Panel from "./Mobilizing/renderer/3D/three/ui/Panel";
import PanelStack from "./Mobilizing/renderer/3D/three/ui/PanelStack";

//Three related input (using Three Math)
import SpaceNavigator from "./Mobilizing/renderer/3D/three/input/SpaceNavigator";

export const three = {
    RendererThree,
    Color,
    Vector2,
    Vector3,
    Matrix3,
    Matrix4,
    Line3,
    Ray,
    Euler,
    Quaternion,
    Rect,

    Camera,
    Light,
    Transform,
    Material,
    CubeMapCamera,

    Texture,
    VideoTexture,
    RenderTexture,
    ImageSequenceTexture,

    Mesh,
    Box,
    Sphere,
    Node,
    Cube,
    Icosahedron,
    Octahedron,
    Tetrahedron,
    Dodecahedron,
    Cylinder,
    Circle,
    Plane,

    MeshImport,

    EdgesMesh,
    Line,
    Segments,
    Points,
    Arc,
    Quad,
    Sprite,
    Text,
    TextOpentype,
    TextTroika,

    Clickable,
    Button,
    ButtonGroup,
    Panel,
    PanelStack,

    SpaceNavigator,
    DualShock4,
}

//Physic
export { default as PhysicsEngine } from "./Mobilizing/renderer/3D/three/physics/PhysicsEngine";

