import Renderer from './Renderer';

export default class Buffer {
    /**
    Buffer Class. Encapsulates the concept of buffer of Audio data. You can load, depending on the platform, various audio file formats, like WAV, MP3 or OGG. See the Web Audio API documentation. Requires the Web Audio API.
    @param {Object} params the parameters object
    @param {RendererAudio} [params.renderer=new Renderer()] The audio renderer
    @param {ArrayBuffer} params.arrayBuffer the raw arraybuffer, already loaded, to give to the audio context
    @param {Function} params.decodedCallback the callback function to call when Audio Context has decoded the sound file's raw  arrayBuffer
    */
    constructor({
        renderer = new Renderer(),
        arrayBuffer = undefined,
        decodedCallback = undefined,
    } = {}) {
        this.renderer = renderer;
        this.arrayBuffer = arrayBuffer;
        this.decodedCallback = decodedCallback;

        if (this.arrayBuffer) {
            this.renderer.audioContext.decodeAudioData(this.arrayBuffer, (buffer) => {
                this.nativeAudioBuffer = buffer;
                this.decodedCallback();
            });
        }
    }

    /**
    * Return the underlying WebAudio audio buffer
    * @return {Buffer} webAudio buffer
    */
    getNativeBuffer() {
        return this.nativeAudioBuffer;
    }
}
