import * as THREE from 'three';
import Mesh from "../Mesh";
import Transform from "../../scene/Transform";

export default class Plane extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.width=1] the plane width in world unit
    * @param {Number} [params.height=1] the plane height in world unit
    * @param {Number} [params.segments=1] tesselation size for all sides
    * @param {Number} [params.widthSegments=1] horizontal tesselation size, overrides segments parameters for this side
    * @param {Number} [params.heightSegments=1] vertical tesselation size, overrides segments parameters for this side
    */
    constructor({
        width = 1,
        height = 1,
        segments = 1,
        widthSegments = undefined,
        heightSegments = undefined,
    } = {}) {
        super(...arguments);

        this.width = width;
        this.height = height;
        this.segments = segments;
        this.widthSegments = widthSegments ? widthSegments : this.segments;
        this.heightSegments = heightSegments ? heightSegments : this.segments;

        this.geometry = new THREE.PlaneGeometry(this.width, this.height, this.widthSegments, this.heightSegments);

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
