import * as THREE from 'three';
import Mesh from "../Mesh";
import Vector2 from "../../types/Vector2";
import Transform from "../../scene/Transform";

export default class Quad extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.point1=Vector2(-1,  1)] upper left point
    * @param {Number} [params.point2=Vector2( 1,  1)] upper right point
    * @param {Number} [params.point3=Vector2( 1, -1)] lower right point
    * @param {Number} [params.point4=Vector2(-1, -1)] lower left point
    */
    constructor({
        point1 = new Vector2(-1, 1),
        point2 = new Vector2(1, 1),
        point3 = new Vector2(1, -1),
        point4 = new Vector2(-1, -1),
    } = {}) {
        super(...arguments);

        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.point4 = point4;

        const rectShape = new THREE.Shape();
        rectShape.moveTo(this.point1.x, this.point1.y);
        rectShape.lineTo(this.point2.x, this.point2.y);
        rectShape.lineTo(this.point3.x, this.point3.y);
        rectShape.lineTo(this.point4.x, this.point4.y);
        rectShape.lineTo(this.point1.x, this.point1.y);

        this.geometry = new THREE.ShapeGeometry(rectShape);

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
