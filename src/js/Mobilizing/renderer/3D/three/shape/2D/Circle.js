import * as THREE from 'three';
import Mesh from "../Mesh";
import Transform from "../../scene/Transform";

export default class Circle extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.radius=1] the circle radius in world unit
    * @param {Number} [params.segments=32] horizontal tesselation size
    * @param {Number} [params.startAngle=0] vertical angle start of the circle
    * @param {Number} [params.endAngle=Math.PI*2] vertical length of the circle
    */
    constructor({
        radius = 1,
        segments = 32,
        startAngle = 0,
        endAngle = Math.PI * 2,
    } = {}) {
        super(...arguments);

        this.radius = radius;
        this.segments = segments;
        this.startAngle = startAngle;
        this.endAngle = endAngle;

        this.geometry = new THREE.CircleGeometry(this.radius, this.segments, this.startAngle, this.endAngle);

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
