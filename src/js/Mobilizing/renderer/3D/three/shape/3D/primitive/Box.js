import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from "../../../scene/Transform";

export default class Box extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.width=1] the box width in world unit
    * @param {Number} [params.height=1] the box height in world unit
    * @param {Number} [params.depth=1] the box depth in world unit
    * @param {Number} [params.segments=1] tesselation size for all sides
    * @param {Number} [params.widthSegments=1] horizontal tesselation size, overrides segments parameters for this side
    * @param {Number} [params.heightSegments=1] vertical tesselation size, overrides segments parameters for this side
    * @param {Number} [params.depthSegments=1] depth tesselation size, overrides segments parameters for this side
    */
    constructor({
        width = 1,
        height = 1,
        depth = 1,
        segments = undefined,
        widthSegments = 1,
        heightSegments = 1,
        depthSegments = 1,
    } = {}) {
        super(...arguments);

        this.width = width;
        this.height = height;
        this.depth = depth;
        this.segments = segments;
        this.widthSegments = widthSegments ? widthSegments : this.segments;
        this.heightSegments = heightSegments ? heightSegments : this.segments;
        this.depthSegments = depthSegments ? depthSegments : this.segments;

        this.geometry = new THREE.BoxGeometry(this.width, this.height, this.depth, this.widthSegments, this.heightSegments, this.depthSegments);

        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;
        
        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
