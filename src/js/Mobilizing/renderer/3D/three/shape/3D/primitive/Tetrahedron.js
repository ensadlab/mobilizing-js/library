import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from "../../../scene/Transform";

export default class Tetrahedron extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.radius=1] the icosahedron radius in world unit
    * @param {Number} [params.detail=0] the icosahedron number of points (tesselation and "roundness")
    */
    constructor({
        radius = 1,
        detail = 0,
    } = {}) {
        super(...arguments);

        this.radius = radius;
        this.detail = detail;

        this.geometry = new THREE.TetrahedronGeometry(this.radius, this.detail);
        
        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
