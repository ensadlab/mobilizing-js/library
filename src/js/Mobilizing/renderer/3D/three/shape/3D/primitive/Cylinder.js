import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from "../../../scene/Transform";

export default class Cylinder extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.radius=1] the cylinder top and bottom radius in world unit
    * @param {Number} [params.radiusTop=1] the cylinder top radius in world unit
    * @param {Number} [params.radiusBottom=1] the cylinder bottom radius in world unit
    * @param {Number} [params.height=1] the cylinder height in world unit
    * @param {Number} [params.radiusSegments=32] horizontal tesselation size
    * @param {Number} [params.heightSegments=1] vertical tesselation size
    * @param {Number} [params.openEnded=false] close the ends of the shape or not
    * @param {Number} [params.thetaStart=0] vertical angle start of the cylinder
    * @param {Number} [params.thetaLength=Math.PI*2] vertical length of the cylinder
    */
    constructor({
        radius = undefined,
        radiusTop = 1,
        radiusBottom = 1,
        height = 1,
        radiusSegments = 32,
        heightSegments = 1,
        openEnded = false,
        thetaStart = 0,
        thetaLength = Math.PI * 2,
    } = {}) {
        super(...arguments);

        this.radius = radius;
        this.radiusTop = radiusTop;
        this.radiusBottom = radiusBottom;
        this.height = height;
        this.radiusSegments = radiusSegments;
        this.heightSegments = heightSegments;
        this.openEnded = openEnded;
        this.thetaStart = thetaStart;
        this.thetaLength = thetaLength;

        if(this.radius){
            this.radiusTop = this.radius;
            this.radiusBottom = this.radius;
        }

        this.geometry = new THREE.CylinderGeometry(this.radiusTop, this.radiusBottom, this.height, this.radiusSegments, this.heightSegments, this.openEnded, this.thetaStart, this.thetaLength);

        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;
        
        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
