//import * as THREE from 'three';
import { Text } from 'troika-three-text'
import Mesh from "../../Mesh";
import Transform from '../../../scene/Transform';
import Vector3 from '../../../types/Vector3';
import Color from '../../../types/Color';
//import Material from '../../../scene/Material';

//https://protectwise.github.io/troika/troika-three-text/
export default class TextTroika extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.text="text"] the text string
    * @param {Number} [params.fontSize=1] the text font height
    * @param {Number} [params.depth=1] the glyph extrusion height. 0 to have flat letters.
    * @param {Number} [params.bevelSegments=5] the text bevel details and tesselation
    * @param {Boolean} [params.bevelEnabled=false] the text geometry uses curves
    * @param {Number} [params.bevelSize=1] the text geometry bevel
    * @param {Number} params.font the text font to use.
    */
    constructor({
        text = "text",
        fontSize = 1,
        fontFile = undefined,
        letterSpacing = 0,
        lineHeight = 'normal',
        maxWidth = Infinity,
        direction = 'auto',
        textAlign = 'left',
        textIndent = 0,
        whiteSpace = 'normal',
        fillOpacity = 1,
        strokeWidth = 0,
        strokeColor = "black",
        strokeOpacity = 1,
        outlineWidth = 0,
        outlineColor = "black",
        outlineOpacity = 1,
        outlineOffsetX = 0,
        outlineOffsetY = 0,
        outlineBlur =  0,
        overflowWrap = 'normal',
        anchorX = 0,
        anchorY = 0,
        curveRadius = 0,
        center = false,
        /* includeCaretPositions = true, //TODO parameterize
        sdfGlyphSize = 64, */
    } = {}) {
        super(...arguments);

        this.text = text;
        this.fontSize = fontSize;
        this.fontFile = fontFile;
        this.letterSpacing = letterSpacing;
        this.lineHeight = lineHeight;
        this.maxWidth = maxWidth;
        this.direction = direction;
        this.textAlign = textAlign;
        this.textIndent = textIndent;
        this.whiteSpace = whiteSpace;
        this.fillOpacity = fillOpacity;
        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;
        this.strokeOpacity = strokeOpacity;
        this.outlineWidth = outlineWidth;
        this.outlineColor = outlineColor;
        this.outlineBlur = outlineBlur;
        this.outlineOpacity = outlineOpacity;
        this.outlineOffsetX = outlineOffsetX;
        this.outlineOffsetY = outlineOffsetY;
        this.overflowWrap = overflowWrap;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.curveRadius = curveRadius;
        //includeCaretPositions = true, //TODO parameterize
        //sdfGlyphSize = 64,
        this.center = center;
        this.colorRanges = null;

        this._mesh = new Text();
        this._mesh.text = this.text;
        this._mesh.font = this.fontFile;
        this._mesh.fontSize = this.fontSize;
        this._mesh.letterSpacing = this.letterSpacing;
        this._mesh.lineHeight = this.lineHeight;
        this._mesh.maxWidth = this.maxWidth;
        this._mesh.direction = this.direction;
        this._mesh.textAlign = this.textAlign;
        this._mesh.textIndent = this.textIndent;
        this._mesh.whiteSpace = this.whiteSpace;
        this._mesh.fillOpacity = this.fillOpacity;
        this._mesh.outlineWidth = this.outlineWidth;
        this._mesh.outlineColor = this.outlineColor;
        this._mesh.outlineOpacity = this.outlineOpacity;
        this._mesh.outlineBlur = this.outlineBlur;
        this._mesh.strokeWidth = this.strokeWidth;
        this._mesh.strokeColor = this.strokeColor;
        this._mesh.strokeOpacity = this.strokeOpacity;
        this._mesh.outlineOffsetX = this.outlineOffsetX;
        this._mesh.outlineOffsetY = this.outlineOffsetY;
        this._mesh.overflowWrap = this.overflowWrap;
        this._mesh.anchorX = this.anchorX;
        this._mesh.anchorY = this.anchorY;
        this._mesh.curveRadius = this.curveRadius;

        //console.log("troikaText", this._mesh, this._mesh.material);

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);
        this._mesh.material = this.material.getNativeObject();
        //console.log("this.material", this.material);

        this.geometry = this._mesh.geometry;
        //console.log("this.geometry", this.geometry);
        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;

        this.transform = new Transform(this);
        this.centerOffset = new Vector3();

        this._mesh.sync(() => this.constructedCallback());
    }

    setText(text) {
        this.text = text;
        this._mesh.text = this.text;
        this._mesh.sync(() => this.syncCallBack());
    }

    setColorForRange(start, end, color) {

        if (!this.colorRanges) {
            this.colorRanges = { 0: Color.white.clone() };
        }

        this.colorRanges[start] = color;

        if (!this.colorRanges[end]) {
            this.colorRanges[end] = Color.white.clone();
        }
        this._mesh.colorRanges = this.colorRanges;
        //console.log(this._mesh.colorRanges);
    }

    constructedCallback() {
        if (this.center) {
            const { x, y } = this.getCenter();
            this.centerOffset.x = -x;
            this.centerOffset.y = -y;
        }
        this.events.trigger("constructed");
    }

    sync(){
        this._mesh.sync();
    }

    syncCallBack() {
        //console.log("syncCallBack");
        this.events.trigger("update");
    }

}
