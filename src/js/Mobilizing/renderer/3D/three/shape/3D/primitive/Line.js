import * as THREE from 'three';
import Mesh from "../../Mesh";
import Vector3 from "../../../types/Vector3";
import Transform from "../../../scene/Transform";
import Material from "../../../scene/Material";


export default class Line extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.point=Vector3(0, 0, 0)] the point 3D coordinates
    */
    constructor({
        point1 = new Vector3(0, -1, 0),
        point2 = new Vector3(0, 1, 0),
    } = {}) {
        super(...arguments);
        
        this.setPoints(point1, point2);

        this.material = new Material({ type: "line" });
        this._mesh = new THREE.Line(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }

    /**
    * Set the coordinates of the 2 points of the line
    * @param {Vector3} point1
    * @param {Vector3} point2
    */
    setPoints(point1, point2) {

        //this.preparePoints(point1, point2)
        this.point1 = point1;
        this.point2 = point2;

        this.setVertices([this.point1, this.point2]);
    }

    /**
   * Get the coordinates of the 2 points of the line
   * @returns {Object} {point1, point2}
   */
    getPoints() {
        return { "point1": this.point1, "point2": this.point2 };
    }
}
