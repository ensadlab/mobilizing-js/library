import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from "../../../scene/Transform";

/**
* A node is an empty 3D object. It has no geometry, but has a tansform, therefore it can be used to create the root element for grouping objects. In Mobilizing, transforms represents a kind of invisible coordinate axis that can be tansformed in space. We use it to construct parent/child relationships, like in `object.transform.addChild(otherObject.transform)`
*/
export default class Node extends Mesh {
    constructor({
        threeObject3D = null,
    } = {}) {
        super(...arguments);

        this.threeObject3D = threeObject3D;
        if (this.threeObject3D) {
            this._mesh = this.threeObject3D;
        }
        else {
            this._mesh = new THREE.Mesh();
        }
        this.transform = new Transform(this);
    }
}
