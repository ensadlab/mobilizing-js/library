import * as THREE from 'three';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import Mesh from '../../Mesh';
import { getFileExtension } from '../../../../../../core/util/Misc';
import * as debug from '../../../../../../core/util/Debug';

export default class MeshImport {

    static importFromFile({
        file = undefined,
        url = undefined
    } = {}) {

        this.file = file;
        this.url = url;
        this.fileExtension = getFileExtension(url);

        this.mesh = undefined;

        debug.log(this.fileExtension);

        switch (this.fileExtension) {
            case ".svg":
                return MeshImport.importSVG(this.file);
            case ".obj":
                return MeshImport.importOBJ(this.file);
            default:
                return null;
        }
    }

    static importSVG(file) {

        const svg = new SVGLoader();
        console.log(svg.parse(file));

        const shapes = svg.parse(file).paths;

        const mesh = new Mesh({ type: "node" });

        shapes.forEach((shapePath) => {

            const shape = SVGLoader.createShapes(shapePath);
            console.log(shape);

            const geometry = new THREE.ShapeGeometry(shape);
            const submesh = new Mesh({
                type: "custom",
                material: "line",
                geometry,
            });
            submesh.setScale(1, -1, 1);
            mesh.transform.addChild(submesh.transform);
            submesh.material.setWireframe(true);
        });

        console.log(mesh);
        return mesh
    }

    static importOBJ(file) {

        const obj = new OBJLoader().parse(file);
        console.log(obj);

        if (obj.children.length >= 1 && obj.children.length < 2) {
            const threeMesh = obj.children[0];
            const mesh = new Mesh({
                type: "custom",
                geometry: threeMesh.geometry
            });
            return mesh;
        }
        else if(obj.children.length > 1){

            const mesh = new Mesh({ type: "node" });
            obj.children.forEach((threeMesh) => {
                const submesh = new Mesh({
                    type: "custom",
                    geometry : threeMesh.geometry
                });
                mesh.transform.addChild(submesh.transform);
            });
            return mesh;
        }
        return null;
    }

}
