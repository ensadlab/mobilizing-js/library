import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from '../../../scene/Transform';

export default class Sphere extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.radius=1] the sphere radius in world unit
    * @param {Number} [params.segments=8] tesselation size for all axis
    * @param {Number} [params.widthSegments=8] horizontal tesselation size
    * @param {Number} [params.heightSegments=8] vertical tesselation size
    * @param {Number} [params.phiStart=0] horizontal angle start of the sphere
    * @param {Number} [params.phiLength=Math.PI*2] horizontal length of the sphere
    * @param {Number} [params.thetaStart=0] vertical angle start of the sphere
    * @param {Number} [params.thetaLength=Math.PI] vertical length of the sphere
    */
    constructor({
        radius = 1,
        segments = 8,
        widthSegments = 8,
        heightSegments = 8,
        phiStart = 0,
        phiLength = Math.PI * 2,
        thetaStart = 0,
        thetaLength = Math.PI,
    } = {}) {
        super(...arguments);

        this.radius = radius;
        this.segments = segments;
        this.widthSegments = segments ? segments : widthSegments;
        this.heightSegments = segments ? segments : heightSegments;
        this.phiStart = phiStart;
        this.phiLength = phiLength;
        this.thetaStart = thetaStart;
        this.thetaLength = thetaLength;

        this.geometry = new THREE.SphereGeometry(this.radius, this.widthSegments, this.heightSegments, this.phiStart, this.phiLength, this.thetaStart, this.thetaLength);

        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
