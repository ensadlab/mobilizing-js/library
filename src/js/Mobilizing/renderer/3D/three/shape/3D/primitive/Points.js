import * as THREE from 'three';
import Mesh from "../../Mesh";
import Material from "../../../scene/Material";
import Transform from "../../../scene/Transform";
import Color from "../../../types/Color";

export default class Point extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.point=Vector3(0, 0, 0)] the point 3D coordinates
    */
     constructor({
        points = [],
        colors = undefined
    } = {}) {
        super(...arguments);

        this.points = points;
        this._points = [];

        this.colors = colors;
       
        if (this.maxVertices < this.points.length) {
            this.setMaxVertices(this.points.length);
        }

        this.material = new Material({ type: "points" });

        //vertex coloring is used
        this.material.getNativeObject().vertexColors = true;

        this._mesh = new THREE.Points(this.geometry, this.material._material);
        this.transform = new Transform(this);

        this.points.forEach((point, index) => {
            this.addPoint(point, index);
        });

        this.setVertices(this._points);
        this.geometry.setIndex(this.indices);

        if (this.colors) {
            this.geometry.attributes.color.set(this.colors);
        }
    }

    setPointsSize(size){
        this.material.setPointSize(size);
    }

    addPoint(point, index) {
        this._points.push(point);
        this.indices.push(index);
        if (!this.colors) {
            this.geometry.attributes.color.setXYZ(index, 1, 1, 1);
        }
    }

    /**
    * Set the coordinates of the points at index of the line
    * @param {Number} index
    * @param {Vector3} point
    */
    setPoint(index, vector) {

        this._points[index] = vector;
        //this.setVerticesFromVectorsArray(this._points);
        this.setVertex(index, vector);
        this.geometry.computeBoundingBox();
    }

    /**
    * Set the color of the points at index of the line
    * @param {Number} index
    * @param {Vector3} point
    */
    setPointColor(index, color) {

        this.geometry.attributes.color.setXYZ(index, color.r, color.g, color.b);
        this.geometry.attributes.color.needsUpdate = true;
    }

    /**
    * Get the array of points
    * @returns {Array} points array
    */
    getPoints() {
        return this._points;
    }

    /**
     * 
     * @returns 
     */
    getPointsColor() {
        const colors = [];
        this.geometry.attributes.color.copyColorsArray(colors);
        return this.geometry.attributes.color.array;
    }

    /**
     * 
     * @param {Number} index 
     * @returns {Color} Color
     */
    getPointColor(index) {
        const r = this.geometry.attributes.color.getX(index);
        const g = this.geometry.attributes.color.getY(index);
        const b = this.geometry.attributes.color.getZ(index);
        return new Color(r, g, b);
    }
}
