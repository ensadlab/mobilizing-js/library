import * as THREE from 'three';
import { TextGeometry } from "three/examples/jsm/geometries/TextGeometry";
import { Font } from "three/examples/jsm/loaders/FontLoader";
import Mesh from "../../Mesh";
import Transform from '../../../scene/Transform';

/**
* 3D Text is created from a typeface represented in JSON, please use <a href="http://gero3.github.io/facetype.js/">facetype.js</a> to generate a JSON from a font file. The generated JSON must be loader with a Loader.load.JSON() method and passed to the constructor.
*/
export default class Text extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.text="text"] the text string
    * @param {Number} [params.fontSize=1] the text font height
    * @param {Number} [params.depth=1] the glyph extrusion depth. 0 to have flat letters.
    * @param {Number} [params.segments=5] the text details and tesselation (curveSegments)
    * @param {Boolean} [params.bevelEnabled=5] the text geometry uses curves
    * @param {Number} params.font the text font to use. Must be a json generated from facetype.js
    */
    constructor({
        text = "text",
        fontSize = 1,
        depth = 1,
        segments = 5,
        bevelEnabled = false,
        bevelSegments = 1,
        bevelThickness = 1,
        bevelSize = 1,
        font = undefined,
    } = {}) {
        super(...arguments);

        this.text = text;
        this.fontSize = fontSize;
        this.depth = depth;
        this.segments = segments;
        this.bevelEnabled = bevelEnabled;
        this.bevelSegments = bevelSegments;
        this.bevelThickness = bevelThickness;
        this.bevelSize = bevelSize;
        this.font = font;

        const textExtrudeSettings = {
            size: this.fontSize,
            height: this.depth,
            curveSegments: this.segments,
            bevelEnabled: this.bevelEnabled,
            bevelThickness: this.bevelThickness,
            bevelSize: this.bevelSize,
            bevelSegments: this.bevelSegments,
            font: new Font(this.font),
        }

        this.geometry = new TextGeometry(this.text, textExtrudeSettings);

        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;

        //this.geometry.center();

        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);
        //console.log(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
