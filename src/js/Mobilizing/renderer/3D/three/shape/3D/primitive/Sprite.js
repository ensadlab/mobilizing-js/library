import * as THREE from 'three';
import Mesh from "../../Mesh";
import Material from '../../../scene/Material';
import Transform from '../../../scene/Transform';

export default class Sprite extends Mesh {
    constructor() {
        super(...arguments);

        this.material = new Material({ type: "sprite" });
        this._mesh = new THREE.Sprite(this.material._material);
        this.transform = new Transform(this);
    }
}
