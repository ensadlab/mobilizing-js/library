import * as THREE from 'three';
import Mesh from "../../Mesh";
import Transform from "../../../scene/Transform";

export default class Cube extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} [params.size=1] the cube dimensions in world unit
    * @param {Number} [params.segments=1] tesselation size for all sides
    */
    constructor({
        size = 1,
        segments = 1
    } = {}) {
        super(...arguments);

        this.size = size;
        this.segments = segments;

        this.geometry = new THREE.BoxGeometry(this.size, this.size, this.size, this.segments, this.segments, this.segments);

        this.geometryVerticesAttributes = this.geometry.attributes.position;
        this.geometryNormalAttributes = this.geometry.attributes.normal;
        this.geometryUVAttributes = this.geometry.attributes.uv;
        
        //manage the material according to the passed params, see the attachMaterial method below
        this.attachMaterial(this.material);

        this._mesh = new THREE.Mesh(this.geometry, this.material._material);
        this.transform = new Transform(this);
    }
}
