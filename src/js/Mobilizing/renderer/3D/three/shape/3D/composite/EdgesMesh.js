import { LineSegments } from "three";
import { EdgesGeometry } from "three";
import Material from "../../../scene/Material";
import Transform from "../../../scene/Transform";
import Mesh from "../../Mesh";

/**
EdgesMesh class creates a hiddenline fashioned Mesh from any other Mesh. "Hard" edges only will be visible. The result is different from when a Mesh material is set to wireframe, as all the segment will not be visible. All the coplanar segment are erased here in a new geometry that is rendered with a "line" type material.
*/
export default class EdgesMesh extends Mesh {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Mesh} params.inputMesh The existing Mesh to use for generate a new edge only Mesh (geometry and material)
    * @param {Number} [params.threshold=1] threshold angle
    */
    constructor({
        inputMesh = undefined,
        threshold = 1,
    } = {}) {
        super(...arguments);

        this.inputMesh = inputMesh;
        this.threshold = threshold;

        this.material = new Material({ type: "line" });

        this._mesh = new LineSegments(new EdgesGeometry(this.inputMesh.geometry, this.threshold), this.material._material);

        this.transform = undefined;
        this.transform = new Transform(this);
    }
}
