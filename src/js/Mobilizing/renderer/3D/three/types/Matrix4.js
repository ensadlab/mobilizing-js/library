import { Matrix4 as THREE_Matrix4 } from 'three';

/**
 * This class extends the one from Three.js, API available here : https://threejs.org/docs/index.html#api/en/math/Matrix4
**/

export default class Matrix4 extends THREE_Matrix4 {
}
