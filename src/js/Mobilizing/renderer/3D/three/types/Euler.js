import { Euler as THREE_Euler } from 'three';

/**
 * This class extends the one from Three.js, API available here : https://threejs.org/docs/index.html#api/en/math/Euler
**/
export default class Euler extends THREE_Euler {
}
