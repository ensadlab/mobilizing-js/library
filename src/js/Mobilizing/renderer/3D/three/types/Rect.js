export default class Rect {
    /**
    * Rect hold a simple rectangle representation with x & y coordinates and width & height.
    * @param {Object} params Parameters object, given by the constructor.
    * @param {float} [params.x = 0]
    * @param {float} [params.y = 0]
    * @param {float} [params.width = 1]
    * @param {float} [params.height = 1]
    */
    constructor({
        x = 0,
        y = 0,
        width = 1,
        height = 1
    } = {}) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
    * Return an array representation
    * @return {Array} [x, y, width, height]
    */
    toArray() {
        return [
            this.x,
            this.y,
            this.width,
            this.height
        ];
    }

    /**
    * Sets the rect's value based on an array formatted like [x, y, width, height]
    * @param {Array} array [x, y, width, height]
    */
    fromArray(array) {
        this.x = array[0];
        this.y = array[1];
        this.width = array[2];
        this.height = array[3];
    }

}
