import { Vector2 as THREE_Vector2 } from 'three';

/**
 * Represents a 2 dimensionnal Euclidean Vector, to be used as 2D positions, 2D directions or 2D Euler angles (rotation).
 *
 * This class extends the one from Three.js, API available here : https://threejs.org/docs/#api/en/math/Vector2
 */
export default class Vector2 extends THREE_Vector2 {

    rotate90CW() {
        const memX = this.x;
        const memY = this.y;
        this.x = memY;
        this.y = -memX;
    }

    rotate90CCW() {
        const memX = this.x;
        const memY = this.y;
        this.x = -memY;
        this.y = memX;
    }
}

/**
* Vector2.one returns <i>new Mobilizing.Vector2(1, 1)</i>
* @property one
* @static
*/
Vector2.one = new Vector2(1, 1);

/**
* Vector2.zero returns <i>new Mobilizing.Vector2(0, 0)</i>
* @property zero
* @static
*/
Vector2.zero = new Vector2(0, 0);
