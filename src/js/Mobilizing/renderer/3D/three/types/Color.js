import { Color as THREE_Color } from 'three';

/**
* Represents a RGB color to be used in materials applied on Meshes.
*
* This class extends the one from Three.js, API available here : https://threejs.org/docs/#api/en/math/Color
* @class Color
*/
export default class Color extends THREE_Color {
    /**
    Creates a color based on [0;1] float RGB values
    @static
    @param {Number} r The red component [0;1].
    @param {Number} g The green component [0;1].
    @param {Number} b The blue component [0;1].
    */
    static Create(r, g, b) {
        return new Color(r, g, b);
    }

    /**
    Creates a random color
    @static
    @return {Color} the created random color
    */
    static random() {
        return new Color(Math.random(), Math.random(), Math.random());
    }

    /**
    * Sets the rgb components of this color to the [0;1] based given value
    * @private
    * @chainable
    * @param {Float} val float number
    * @return {Color} this color with the given value
    */
    setTo(val) {
        this.r = val;
        this.g = val;
        this.b = val;

        return this;
    }

    /**
    Creates a color based on [0;255] RGB values
    @param {Number} r The red component [0;255].
    @param {Number} g The green component [0;255].
    @param {Number} b The blue component [0;255].
    */
    setRGB32(r, g, b) {
        this.r = r / 255;
        this.g = g / 255;
        this.b = b / 255;
    }

    /**
    Build a HTML color string with the given alpha.
    @param {Number} a The alpha component [0;255].
    @return {String} a string representation of the color with alpha
    */
    makeRGBAStringWithAlpha(a) {
        const string = `rgba(${Math.floor(this.r * 255)},${Math.floor(this.g * 255)},${Math.floor(this.b * 255)},${a})`;

        return string;
    }

    /**
    * Copies this color.
    * @return Color
    */
    copy() {
        super.copy();
    }
}

/**
* Color.green, a green Color Object
* @property green {Color}
* @static
*/
Color.green = new Color(0.0, 1.0, 0.0);
/**
* Color.blue, a blue Color Object
* @property blue {Color}
* @static
*/
Color.blue = new Color(0.0, 0.0, 1.0);
/**
* Color.red, a red Color Object
* @property red {Color}
* @static
*/
Color.red = new Color(1.0, 0.0, 0.0);
/**
* Color.white, a white Color Object
* @property white {Color}
* @static
*/
Color.white = new Color(1.0, 1.0, 1.0);
/**
* Color.darkGray, a darkGray Color Object
* @property darkGray {Color}
* @static
*/
Color.darkGray = new Color(0.3, 0.3, 0.3);
/**
* Color.gray, a gray Color Object
* @property gray {Color}
* @static
*/
Color.gray = new Color(0.5, 0.5, 0.5);
/**
* Color.lightGray, a lightGray Color Object
* @property lightGray {Color}
* @static
*/
Color.lightGray = new Color(0.8, 0.8, 0.8);
/**
* Color.black, a black Color Object
* @property black {Color}
* @static
*/
Color.black = new Color(0.0, 0.0, 0.0);

/**
* Color.cyan, a cyan Color Object
* @property cyan {Color}
* @static
*/
Color.cyan = new Color(0, 1, 1);

/**
* Color.magenta, a magenta Color Object
* @property magenta {Color}
* @static
*/
Color.magenta = new Color(1, 0, 1);

/**
* Color.yellow, a yellow Color Object
* @property yellow {Color}
* @static
*/
Color.yellow = new Color(1, 1, 0);


/**
* Color.lightRed, a lightRed Color Object
* @property lightRed {Color}
* @static
*/
Color.lightRed = new Color(1, 0.5, 0.5);

/**
* Color.lightGreen, a lightGreen Color Object
* @property lightGreen {Color}
* @static
*/
Color.lightGreen = new Color(0.5, 1, 0.5);

/**
* Color.lightBlue, a lightBlue Color Object
* @property lightBlue {Color}
* @static
*/
Color.lightBlue = new Color(0.5, 0.5, 1);

Color.mobilizing = new Color(60 / 255, 30 / 255, 1);
Color.mobilizingDark = new Color(36 / 255, 18 / 255, 153 / 255);
Color.mobilizingLight = new Color(208 / 255, 202 / 255, 247 / 255);
Color.mobilizingAlternate = new Color(255 / 255, 158 / 255, 116 / 255);

/**
* Color.transparent, a transparent Color Object
* @property transparent {Color}
* @static
*/
Color.transparent = new Color(new Color().makeRGBAStringWithAlpha(0));
