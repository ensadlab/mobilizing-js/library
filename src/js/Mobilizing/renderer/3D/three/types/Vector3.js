import { Vector3 as THREE_Vector3 } from 'three';

/**
 * Represents a 3 dimensionnal Euclidean Vector, to be used as positions, directions or Euler angles (rotation).
 *
 * This class extends the one from Three.js, API available here : https://threejs.org/docs/#api/en/math/Vector3
 * @class Vector3
 */

export default class Vector3 extends THREE_Vector3 {
}

/**
* Vector3.one returns <i>new Mobilizing.Vector2(1, 1, 1)</i>
* @property one
* @static
*/
Vector3.one = new Vector3(1, 1, 1).clone();

/**
* Vector3.one returns <i>new Mobilizing.Vector2(0, 0, 0)</i>
* @property zero
* @static
*/
Vector3.zero = new Vector3(0, 0, 0).clone();

