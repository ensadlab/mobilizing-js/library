import { Line3 as THREE_Line3 } from 'three';
import Vector2 from "../types/Vector2";

export default class Line3 extends THREE_Line3 {

    /**
     * For 2D lines only, ie lines with Z value to 0
     * @param {Line3} line 
     * @returns {Vector2}
     */
    intersectionPoint(line) {
        const a1 = this.end.x - this.start.x;
        const b1 = line.start.x - line.end.x;
        const c1 = line.start.x - this.start.x;

        const a2 = this.end.y - this.start.y;
        const b2 = line.start.y - line.end.y;
        const c2 = line.start.y - this.start.y;

        const t = (b1 * c2 - b2 * c1) / (a2 * b1 - a1 * b2);

        const point = new Vector2();
        point.x = this.start.x + t * (this.end.x - this.start.x);
        point.y = this.start.y + t * (this.end.y - this.start.y);
        return point;
    }
}

