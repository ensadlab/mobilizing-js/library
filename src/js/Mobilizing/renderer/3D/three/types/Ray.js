import { Ray as THREE_Ray } from 'three';
import { Box3 } from 'three';

/**
 * A ray that emits from an origin in a certain direction.
 */
export default class Ray extends THREE_Ray {
    /**
    * Test intersection between this ray and a mesh bounding box
    * @param {Mesh} mesh the mesh to test the ray with
    * @return {Boolean} intersection result
    */
    intersectsMeshBox(mesh) {
        const bbox = new Box3();
        bbox.setFromObject(mesh.mesh);

        return this.intersectsBox(bbox);
    }
}
