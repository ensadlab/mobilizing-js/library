import { Matrix3 as THREE_Matrix3 } from 'three';

/**
 * This class extends the one from Three.js, API available here : https://threejs.org/docs/index.html#api/en/math/Matrix3
**/

export default class Matrix3 extends THREE_Matrix3 {
}
