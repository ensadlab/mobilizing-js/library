import * as THREE from 'three';

export default class RenderTexture {
    /**
    * RenderTexture are 3D camera rendered bitmap pictures loaded in the graphic card memory so they can be mapped on a geometry surface. Usually, texture are mapped through a Material. Keep in mind that WebGL, as an implementation of OpenGL, shares the same kind of limitations : textures sources should be power of 2 (even if here, Three.js is doing the job of resizing for you if they're not).
    *
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Number} params.width the width of this texture
    * @param {Number} params.height the height of this texture
    * @param {String} params.format the texture format to use (see https://threejs.org/docs/#api/constants/Textures > Formats)
    */
    constructor({
        type = null,
        format = "RGBFormat",
        width = 512,
        height = 512,
    } = {}) {
        this.type = type;
        this.format = format;
        this.width = width;
        this.height = height;

        this.renderTarget = new THREE.WebGLRenderTarget(this.width, this.height, {
            "minFilter": THREE.LinearFilter,
            "magFilter": THREE.NearestFilter,
            "format": THREE[this.format]
        });
        this.texture = this.renderTarget.texture;
    }

    /**
    * @returns the Three.js native object used in this class
    */
    getNativeObject() {
        return this.texture;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

}
