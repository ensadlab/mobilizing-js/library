import * as THREE from "three";
import Transform from "./Transform";

/**
 * Simple wrapper around Three Scene. Should never be used by user. Internal usage only!
 */
export default class Scene {

    constructor({
        name = undefined
    } = {}) {

        this.name = name;
        this._scene = new THREE.Scene();
        this.transform = new Transform(this);
    }

    /**
    * @returns the Three.js native object used in this class
    */
    getNativeObject() {
        return this._scene;
    }

    setVisible(val){
        this._scene.visible = val;
    }

    getVisible(){
        return this._scene.visible;
    }

    setAutoUpdate(val){
        this._scene.autoUpdate = val;
    }

    getAutoUpdate(){
        return this._scene.autoUpdate;
    }

    setBackground(object){
        this._scene.background = object;
    }

    getBackground(){
        return this._scene.background;
    }

    setOverrideMaterial(material){
        this._scene.overrideMaterial = material;
    }
}
