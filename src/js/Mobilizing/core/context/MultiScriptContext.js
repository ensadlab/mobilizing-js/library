import RendererThree from "../../renderer/3D/RendererThree";
import Camera from "../../renderer/3D/three/scene/Camera";
import Light from "../../renderer/3D/three/scene/Light";
//import Time from "../../time/Time";
import Mouse from "../../input/Mouse";
import Touch from "../../input/Touch";
import Pointer from "../../input/Pointer";
import Context from "../Context";
import CubeMapCamera from "../../renderer/3D/three/scene/CubeMapCamera";
import Node from "../../renderer/3D/three/shape/3D/primitive/Node";

/**
* A class to instanciate a Mobilizing Context with mutliple script support. In this context, 1 ThreeRenderer and 1 AudioRenderer are shared with multiple user script. One 3D Scene is used for all the user scripts that are attached to this context.
* @class MultiScriptContext
* @extends {Context}
*/
export default class MultiScriptContext extends Context {

    constructor({
        renderer = true,
        antialias = false,
        rendererAudio = false,
        mouse = true,
        touch = true,
        pointer = true,
        camera = true,
        cubeCamera = false,
        cubeCameraTypeFace = null,
        cubeCameraMakeTestScene = false,
        customCameraNode = false,
        light = false,
    } = {}) {

        super(...arguments);

        this.renderer = renderer;
        this.antialias = antialias;
        this.rendererAudio = rendererAudio;
        this.mouse = mouse;
        this.touch = touch;
        this.pointer = pointer;
        this.camera = camera;
        this.cubeCamera = cubeCamera;
        this.cubeCameraTypeFace = cubeCameraTypeFace;
        this.cubeCameraMakeTestScene = cubeCameraMakeTestScene;
        this.customCameraNode = customCameraNode;
        this.light = light;

        //this is used only for the Mobilizing platform, to manage multi-script sharing the same renderer
        this.rootNodes = {};

        this.scripts = [];

        this.setup();
    }

    setup() {
        if (!this._setupDone) {

            if (this.argIsBoolean(this.renderer)) {
                if (this.renderer) {
                    this.renderer = new RendererThree({
                        context: this,
                        antialias : this.antialias,
                    });
                    this.addComponent(this.renderer);
                }
            }
            else {
                if (this.renderer instanceof RendererThree) {
                    this.addComponent(this.renderer);
                }
            }

            //setup what we need to have touch/mouse
            if (this.argIsBoolean(this.mouse)) {
                if (this.mouse) {
                    this.mouse = new Mouse({
                        target: this.renderer.canvas
                    });
                    this.addComponent(this.mouse);
                    this.mouse.setup();
                    this.mouse.on();//active it
                }
            }
            else {
                if (this.mouse instanceof Mouse) {
                    this.addComponent(this.mouse);
                    this.mouse.setup();
                    this.mouse.on();//active it
                }
            }

            if (this.argIsBoolean(this.touch)) {
                if (this.touch) {
                    this.touch = new Touch({
                        target: this.renderer.canvas
                    });
                    this.addComponent(this.touch);
                    this.touch.setup();
                    this.touch.on();
                }
            }
            else {
                if (this.touch instanceof Touch) {
                    this.addComponent(this.touch);
                    this.touch.setup();
                    this.touch.on();
                }
            }

            if (this.argIsBoolean(this.pointer)) {
                if (this.pointer) {
                    this.pointer = new Pointer();
                    this.addComponent(this.pointer);
                    this.pointer.add(this.mouse);
                    this.pointer.add(this.touch);
                    this.pointer.setup();
                }
            }
            else {
                if (this.pointer instanceof Pointer) {
                    this.addComponent(this.pointer);
                    this.pointer.add(this.mouse);
                    this.pointer.add(this.touch);
                    this.pointer.setup();
                }
            }

            if (this.argIsBoolean(this.camera)) {
                if (this.camera) {
                    this.camera = new Camera();
                    const farPlane = 10000;
                    this.camera.setFarPlane(farPlane);
                    this.renderer.addCamera(this.camera);
                }
            }
            else {
                if (this.camera instanceof Camera) {
                    this.renderer.addCamera(this.camera);
                }
            }
            //cubeMap cam
            if (this.argIsBoolean(this.cubeCamera)) {
                if (this.cubeCamera) {

                    const cubeCamera = new CubeMapCamera({
                        renderer: this.renderer,
                        typeFace: this.cubeCameraTypeFace,
                        makeTestScene: this.cubeCameraMakeTestScene,
                    });
                    this.camera = cubeCamera.makeCubeCameraNode();
                    //console.log("this.camera", this.camera);
                }
            }

            if (this.customCameraNode instanceof Node) {
                this.camera = this.customCameraNode;
            }

            if (this.argIsBoolean(this.light)) {
                if (this.light) {
                    this.light = new Light();
                    //this.light.transform.setLocalPosition(100, 100, 100);
                    const lightDistance = 10000;
                    this.light.setDistance(lightDistance);
                    this.renderer.addToCurrentScene(this.light);
                }
            }
            else {
                if (this.light instanceof Light) {
                    this.renderer.addToCurrentScene(this.light);
                }
            }
        }

        //this will add all scripts to this context renderer scene manager, so that we can call something like `scenes.SketchScript.transform.setLocalPosition(0, -20, 0);` from the main program. "setupStart" events are generated by the runner just before starting the update loop!
        this.events.on("setupStart", (scope) => {
            //console.log("setupStart", scope.name);
            scope.context.renderer.setCurrentScene(scope.name);
        });

        super.setup();
    }

    /**
     * 
     * @param {Object} script 
     */
    addScript(script, options) {
        //console.log("add",script);
        const { useCubeMapCamera } = options;
        const scriptInstance = new script(options);
        //add a layer to be seen by this context camera
        if (!useCubeMapCamera) {
            if (!this.customCameraNode) {
                console.log("use single camera rendrering");
                this.camera.addLayer(scriptInstance.name);
            }
            else {
                this.renderer.addToCurrentScene(this.customCameraNode);
                const cams = this.customCameraNode.transform.getChildren();
                cams.forEach( (cam) => {
                    this.renderer.addCamera(cam);
                    cam.addLayer(scriptInstance.name);
                });
            }
        }

        this.scripts.push(scriptInstance);
        this.addComponent(scriptInstance);

        return scriptInstance;
    }

    /**
     * Can't be used with bundle
     * @param {Array} pathsArray 
     * @returns {Set} script array
     */
    async addScriptsFromPaths(pathsArray) {
        //console.log("location",location);
        const scripts = new Set();
        for (const path of pathsArray) {
            const locationPath = `${location.pathname}${path}`;
            //console.log("locationPath", locationPath);
            scripts.add(import(locationPath).catch((error) => {
                console.log(error);
            }));
        }
        //console.log(scripts);
        const result = await Promise.all(scripts);
        return result;
    }

    /**
     * 
     * @param {Node} node a Node that MUST have a name!
     */
    addRootNode(node) {
        this.rootNodes[node.name] = node;
    }

    /**
     * 
     * @param {String} name the node's name to get from the all the root nodes
     * @return {Node} the root node
     */
    getRootNode(name) {
        return this.rootNodes[name];
    }

    /**
     * 
     * @param {Mixed} arg 
     */
    argIsBoolean(arg) {

        let result = false;

        if (arg === true || arg === false) {
            result = true;
        }
        return result;
    }
}
