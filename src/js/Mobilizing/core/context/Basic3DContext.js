import RendererThree from "../../renderer/3D/RendererThree";
import Camera from "../../renderer/3D/three/scene/Camera";
import Light from "../../renderer/3D/three/scene/Light";
import Time from "../../time/Time";
import Mouse from "../../input/Mouse";
import Touch from "../../input/Touch";
import Pointer from "../../input/Pointer";
import Context from "../Context";

/**
* A class to instanciate a Mobilizing Context with prebuild elements.
These can be access like this in the user script : ```this.context.camera```. It contains these components : ```Time```, ```Touch```, ```Mouse```, ```Pointer``` and these 3D scene elements : ```RendererThree```, ```Camera```, ```Light```.
* @class Basic3DContext
* @extends {Context}
*/
export default class Basic3DContext extends Context {

    setup() {

        //this is ONLY for ESDoc to generate the documention...

        /**
         * @type {RendererThree}
         */
        this.renderer = undefined;
        /**
         * @type {Light}
         */
        this.light = undefined;
        /**
         * @type {Camera}
         */
        this.camera = undefined;
        /**
         * @type {Mouse}
         */
        this.mouse = undefined;
        /**
         * @type {Touch}
         */
        this.touch = undefined;
        /**
         * @type {Pointer}
         */
        this.pointer = undefined;
        /**
         * @type {Time}
         */
        this.time = undefined;

        //internal use of static method
        Basic3DContext.makeBasicContext(this);

        super.setup();
    }

    /**
     * Static method to allow aggregation of BasicContext elements to a standard Context, just by calling this in the setup().
     * @param {Context} ctx the context to use for members addition
     */
    static makeBasicContext(ctx) {

        const context = (ctx !== undefined) ? ctx : this;

        //3D renderer
        context.renderer = new RendererThree();
        context.addComponent(context.renderer);

        //camera
        context.camera = new Camera();
        const farPlane = 10000;
        context.camera.setFarPlane(farPlane);
        context.renderer.addCamera(context.camera);

        //light
        context.light = new Light();
        context.light.transform.setLocalPosition(100, 100, 100);
        const lightDistance = 10000;
        context.light.setDistance(lightDistance);
        context.renderer.addToCurrentScene(context.light);

        //time component
        context.time = new Time();
        context.addComponent(context.time);
        context.time.setup();
        context.time.on();

        //setup what we need to have touch/mouse
        context.mouse = new Mouse({
            "target": context.renderer.canvas
        });
        context.addComponent(context.mouse);
        context.mouse.setup();
        context.mouse.on();//active it

        context.touch = new Touch({
            "target": context.renderer.canvas
        });
        context.addComponent(context.touch);
        context.touch.setup();
        context.touch.on();//active it

        context.pointer = new Pointer();
        context.addComponent(context.pointer);
        context.pointer.add(context.mouse);
        context.pointer.add(context.touch);
        context.pointer.setup();
    }
}
