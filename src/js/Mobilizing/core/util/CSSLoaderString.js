export const CSSString = `
.loaderContainer {
    position: absolute;
    top:0px;
    left: 0px;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: black;
  }
  
  .loader {
    display: inline-flex;
    flex-wrap: wrap;
    width: 90px;
    height: 90px;
    transform-style: preserve-3d;
    transform: rotateX(45deg) rotate(45deg);
  }
  
  .box {
    position: absolute;
    top: 0;
    left: 0;
    width: 15px;
    height: 15px;
    background: #3c1eff;
    -webkit-animation: move 5s linear infinite both;
    animation: move 5s linear infinite both;
    transform-style: preserve-3d;
  }
  .box:nth-child(1) {
    -webkit-animation-delay: -2s;
    animation-delay: -2s;
    width: 13px;
    height: 13px;
  }
  .box:nth-child(2) {
    -webkit-animation-delay: -3s;
    animation-delay: -3s;
    width: 12px;
    height: 12px;
  }
  .box:nth-child(3) {
    -webkit-animation-delay: -4s;
    animation-delay: -4s;
    width: 11px;
    height: 11px;
  }
  .box:nth-child(4) {
    -webkit-animation-delay: -5s;
    animation-delay: -5s;
    width: 10px;
    height: 10px;
  }
  .box:nth-child(5) {
    -webkit-animation-delay: -6s;
    animation-delay: -6s;
    width: 9px;
    height: 9px;
  }
  .box:before,
  .box:after {
    display: block;
    content: "";
    position: absolute;
    width: inherit;
    height: inherit;
  }
  .box:before {
    top: 100%;
    left: 0;
    background: #241299;
    transform-origin: center top;
    transform: rotateX(-90deg);
  }
  .box:after {
    top: 0;
    left: 100%;
    background: #d0caf7;
    transform-origin: center left;
    transform: rotateY(90deg);
  }
  
  @-webkit-keyframes move {
    0%,
    100% {
      transform: none;
    }
    12.5% {
      transform: translate(30px, 0);
    }
    25% {
      transform: translate(60px, 0);
    }
    37.5% {
      transform: translate(60px, 30px);
    }
    50% {
      transform: translate(60px, 60px);
    }
    62.5% {
      transform: translate(30px, 60px);
    }
    75% {
      transform: translate(0, 60px);
    }
    87.5% {
      transform: translate(0, 30px);
    }
  }
  
  @keyframes move {
    0%,
    100% {
      transform: none;
    }
    12.5% {
      transform: translate(30px, 0);
    }
    25% {
      transform: translate(60px, 0);
    }
    37.5% {
      transform: translate(60px, 30px);
    }
    50% {
      transform: translate(60px, 60px);
    }
    62.5% {
      transform: translate(30px, 60px);
    }
    75% {
      transform: translate(0, 60px);
    }
    87.5% {
      transform: translate(0, 30px);
    }
  }
`;
