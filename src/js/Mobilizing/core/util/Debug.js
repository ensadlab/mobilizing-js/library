
/**
 * A simple mapping of console outputs to avoid saturation of console for Mobilizing inner messages.
 To activate the output of this class you must add ?debug=true at the end of the URL of your browser.
 */

let enabled = false;
const url_params = new URLSearchParams(window.location.search);
if (url_params.has('debug') /* && url_params.get('debug') */) {
    enabled = true;
}

export function log() {
    if (enabled) {
        console.log.apply(console, arguments);
    }
}

export function warn() {
    if (enabled) {
        console.warn.apply(console, arguments);
    }
}

export function info() {
    if (enabled) {
        console.info.apply(console, arguments);
    }
}

export function error() {
    if (enabled) {
        console.error.apply(console, arguments);
    }
}
