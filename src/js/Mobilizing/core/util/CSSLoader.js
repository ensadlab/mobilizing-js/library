import { CSSString } from "./CSSLoaderString.js";

export default class CSSLoader {

    constructor() {

        const style = document.createElement('style');
        style.textContent = CSSString;
        document.head.append(style);

        if (document.readyState === 'loading') {
            window.addEventListener("DOMContentLoaded", this.buildElements);
        }
        else {
            this.buildElements();
        }
    }

    buildElements() {
        /*
      <div class="loaderContainer">
          <div class="loader">
              <div class="box"></div>
              <div class="box"></div>
              <div class="box"></div>
              <div class="box"></div>
              <div class="box"></div>
          </div>
      </div>
      */
        const loaderContainer = document.createElement("div");
        loaderContainer.className = "loaderContainer";
        loaderContainer.id = "loaderContainer";
        document.body.appendChild(loaderContainer);

        const loader = document.createElement("div");
        loader.className = "loader";
        loaderContainer.appendChild(loader);

        for (let i = 0; i < 5; i++) {
            const box = document.createElement("div");
            box.className = "box";
            loader.appendChild(box);
        }
    }
}
