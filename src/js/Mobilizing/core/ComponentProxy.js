import Component from './Component';
//import EventEmitter from './util/EventEmitter';

/**
* ComponentProxy is a proxy that converts (like a kind of casting) scripts into Mobilizing Components.
* It was designed specifically to convert users script to regular Components, so they can be used besides all the other Components used by the program.
* The script should implement the methods preLoad(), setup(), update().
* Users can define their own Component using this class for converting their script to a regular Mobilizing Component.
*
* @example
*     //to do
*
* @extends Component
*/
export default class ComponentProxy extends Component {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Script} params.targetScript The Mobilizing user's script to convert in a Component
    */
    constructor({
        targetScript = undefined,
    } = {}) {
        super(...arguments);

        this.targetScript = targetScript;

        //transmit the context to the user script
        this.targetScript.context = this.context;
    }

    prepare(...args) {
        if (typeof this.targetScript.prepare === "function") {
            this.targetScript.prepare(...args);
        }
    }

    /**
    * preLoad the targetScript
    * @param {Rest param} args
    */
    preLoad(...args) {
        if (typeof this.targetScript.preLoad === "function") {
            this.targetScript.preLoad(...args);
        }
    }

    /**
    * Set's up the targetScript
    */
    setup() {
        if (!this._setupDone) {
            super.setup();
            this.context.events.trigger("setupStart", this.targetScript);
            this.targetScript.setup();
        }
    }

    preUpdate() {
        if (typeof this.targetScript.preUpdate === "function") {
            this.targetScript.preUpdate();
        }
    }

    /**
    * Update the targetScript
    */
    update() {
        if (typeof this.targetScript.update === "function") {
            this.targetScript.update();
        }
    }

    postUpdate() {
        if (typeof this.targetScript.postUpdate === "function") {
            this.targetScript.postUpdate();
        }
    }
}
