/* eslint-disable no-unused-vars */
import Component from '../core/Component';
import Gamepad from './Gamepad';

export default class DualShock4 extends Component {

    constructor({
        padSensitivity = 0.05,
        leftPadScaleFactor = 1,
        rightPadScaleFactor = 1,
    } = {}) {
        super(...arguments);

        this.padSensitivity = padSensitivity;
        this.leftPadScaleFactor = leftPadScaleFactor;
        this.rightPadScaleFactor = rightPadScaleFactor;
    }

    setup() {

        if (!this._setupDone) {

            this.dualShock4 = null;
            this.connected = false;

            this.gamepad = new Gamepad();
            this.context.addComponent(this.gamepad);
            this.gamepad.setup();
            this.gamepad.on();

            console.log(this.gamepad);

            this.gamepad.events.on("connected", (gamepad) => {
                console.log(gamepad);
                if (this.isDualShock(gamepad)) {
                    this.dualShock4 = gamepad;
                    this.connected = true;
                    this.events.trigger("connected");
                }
            });

            this.gamepad.events.on("disconnected", (gamepad) => {
                if (this.isDualShock(gamepad)) {
                    this.dualShock4 = null;
                    this.connected = false;
                    this.events.trigger("disconnected");
                }
            });

            this.prevLeftPad = { x: 0, y: 0 };
            this.prevRightPad = { x: 0, y: 0 };

            this.leftPad = { x: 0, y: 0 };
            this.rightPad = { x: 0, y: 0 };

            this.buttonsOrder = ["cross", "circle", "square", "triangle", "L1", "R1", "L2", "R2", "share", "options", "leftPad", "rightPad", "top", "bottom", "left", "right", "ps", "trackpad"];

            this.prevButtons = {};
            this.buttons = {};

            this.buttonsOrder.forEach((button) => {
                this.prevButtons[button] = false;
                this.buttons[button] = false;
            });
        }
        super.setup();
    }

    isDualShock(gamepad) {

        const gamepadName = gamepad.id.toLowerCase();
        console.log("gamepadName", gamepadName);

        if (gamepadName.indexOf("vendor: 054c") >= 0 &&
            (gamepadName.indexOf("product: 09cc") >= 0 || gamepadName.indexOf("product: 05c4") >= 0)) {
            console.log("found DualShock4", gamepad);
            return true;
        }
        return false;
    }

    updatePad(side) {
        if (this.dualShock4) {

            const updatedDualShock4 = this.gamepad.getGamePadFromIndex(this.dualShock4.index);
            this.dualShock4 = updatedDualShock4;

            const axes = this.gamepad.getAllAxes(this.dualShock4.index);

            let tempX = 0;
            let tempY = 0;
            let prevPad = null;
            let pad = null;
            let padScaleFactor = null;

            if (side === "left") {
                tempX = axes[0];
                tempY = axes[1];
                prevPad = this.prevLeftPad;
                pad = this.leftPad;
                padScaleFactor = this.leftPadScaleFactor;
            }
            else if (side === "right") {
                tempX = axes[2];
                tempY = axes[3];
                prevPad = this.prevRightPad;
                pad = this.rightPad;
                padScaleFactor = this.rightPadScaleFactor;
            }

            const diffX = Math.abs(tempX - prevPad.x) > this.padSensitivity;
            const diffY = Math.abs(tempY - prevPad.y) > this.padSensitivity;

            if (diffX) {
                pad.x = tempX * padScaleFactor;
                prevPad.x = tempX;
                this.events.trigger("leftPad", pad);
            }

            if (diffY) {
                pad.y = tempY * padScaleFactor;
                prevPad.y = tempY;
                this.events.trigger("leftPad", pad);
            }

            if (tempX > -this.padSensitivity && tempX < this.padSensitivity) {
                pad.x = 0;
            }
            if (tempY > -this.padSensitivity && tempY < this.padSensitivity) {
                pad.y = 0;
            }
        }
    }

    getLeftPad() {
        return this.leftPad;
    }

    getRightPad() {
        return this.rightPad;
    }

    getButton(name){
        return this.buttons[name];
    }

    update() {

        if (this.dualShock4) {

            const updatedDualShock4 = this.gamepad.getGamePadFromIndex(this.dualShock4.index);
            this.dualShock4 = updatedDualShock4;

            if (this.dualShock4.connected) {

                this.updatePad("left");
                this.updatePad("right");

                //buttons
                const buttons = this.gamepad.getAllButtons(this.dualShock4.index);

                buttons.forEach((button, index) => {

                    const buttonName = this.buttonsOrder[index];

                    if (this.buttons[buttonName] !== this.prevButtons[buttonName]) {
                        this.buttons[buttonName] = button.pressed;
                        //console.log(buttonName, button);
                        this.events.trigger(`${buttonName}`, this.buttons[buttonName]);
                    }

                    this.prevButtons[buttonName] = button.pressed;
                });
            }
        }
    }

}
