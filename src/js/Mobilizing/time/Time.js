import Component from '../core/Component';

/**
* The Time class provides methods to control time related operations. One instance of this Component is created internnaly by the Context. It makes it possible to access and to use a global time in Components that require it (i.e Touch, Animation, etc.). In users script, this global Time can be accessed via the context.
*/
export default class Time extends Component {
    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {Date} params.startTime the Date to start the time at
    * @param {Number} params.scale this time's scale
    * @example
    *    //TODO
    */
    constructor({
        startTime = performance.now(),
        scale = 1,
    } = {}) {
        super(...arguments);

        this.startTime = startTime;
        this.scale = scale;

        /**
         the current time in ms
         @type {Number}
         */
        this.currentTime = performance.now();
        /**
        the time delta in ms
        @type {Number}
        */
        this.delta = 0;
    }

    /**
    * Start the Time
    */
    on() {
        super.on();
        this.reset();
    }

    /**
    * resets this time instance
    */
    reset() {
        this.startTime = performance.now();
    }

    /**
    * updates this time instance
    */
    update() {
        //the component must be on, else we "stop" the time
        if (this.active) {
            const lastTime = this.currentTime;
            this.currentTime = performance.now();
            this.delta = (this.currentTime - lastTime) * this.scale;
        }
    }

    /**
     * returns the diff between current time and past time
     * @returns delta in ms
     */
    getDelta(){
        return this.delta;
    }

    /**
    * absolute Delta
    *
    * @return {Number} absolute Delta in ms
    */
    getAbsoluteDelta() {
        return this.currentTime - this.startTime;
    }

    /**
    * Delta converted in seconds
    *
    * @return {Number} Delta in seconds
    */
    getDeltaSeconds() {
        return this.delta / 1000;
    }
}
