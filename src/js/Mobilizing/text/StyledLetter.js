/**
* StyledLetter is a class that helps to represents a letter with its style for later drawing.
*/
export default class StyledLetter {

    /**
    * @param {Object} params Parameters object, given by the constructor.
    * @param {String} params.letter the letter's string (character)
    * @param {Number} params.size the size of the font
    * @param {Font} params.font a Mobilizing font reference
    * @param {Color} params.color a CSS Color
    */
    constructor({
        letter = "",
        font = undefined,
        size = 40,
        color = "black",
    } = {}) {
        this.letter = letter;
        this.font = font;
        this.size = size;
        this.color = color;

        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;

        if (this.font) {
            this.update();
        }
    }

    /**
    * Updates properties according to the new settings
    */
    update() {
        const size = this.font.getTextSize(this.letter, this.size);
        this.width = size.width;
        this.height = size.height;

        this.path = this.font.getFont().getPath(this.letter, this.x, this.y, this.size);
        this.path.fill = this.color;
        this.boundingBox = this.path.getBoundingBox();
    }

    /**
    * Defines the letter's string
    * @param {String} letter
    */
    setLetter(letter) {
        this.letter = letter;
    }

    /**
    * Defines the letter's x coord
    * @param {Number} x
    */
    setX(x) {
        this.x = x;
    }

    /**
    * Defines the letter's y coord
    * @param {Number} y
    */
    setY(y) {
        this.y = y;
    }

    /**
    * Defines the letter's font size
    * @param {Number} size
    */
    setSize(size) {
        this.size = size;
    }

    /**
    * Defines the letter's color
    * @param {Color} color
    */
    setColor(color) {
        this.color = color;
    }

    /**
    * Defines the letter's font
    * @param {Font} font
    */
    setFont(font) {
        this.font = font;
    }
}
