export default class TextLine {
    /**
    * TextLine is a thin layer class to organise text elements extacted from HTML text in lines. It gives the possibility to easily compute the global size of the line. Used in RichText.
    */
    constructor() {
        this.styledTextElements = [];
        this.width = 0;
    }
}
