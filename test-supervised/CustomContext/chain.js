export class ChainScript {

    preLoad() {
    }

    setup() {
        this.light = new Mobilizing.three.Light();
        this.light.transform.setLocalPosition(100, 100, 0);
        const lightDistance = 10000;
        this.light.setDistance(lightDistance);
        this.context.renderer.addToCurrentScene(this.light);

        const cube = new Mobilizing.three.Cube();
        cube.transform.setLocalScale(10);
        cube.transform.setLocalRotation(10,30,10);
        this.context.renderer.addToCurrentScene(cube);
        
        console.log("current scene", this.context.renderer.getCurrentScene());
    }

    update() {
        //this.context.renderer.setCurrentScene("chain");
    }
}
