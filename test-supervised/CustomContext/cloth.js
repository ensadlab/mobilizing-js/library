export class ClothScript {

    setup() {

        //console.log("setup",this);

        this.light = new Mobilizing.three.Light();
        //this.light.transform.setLocalPosition(100, 100, 100);
        const lightDistance = 10000;
        this.light.setDistance(lightDistance);
        this.context.renderer.addToCurrentScene(this.light);

        this.context.camera.transform.setLocalPositionZ(100);

        this.floor = new Mobilizing.three.Cube(/* {material: "basic"} */);
        this.floor.transform.setLocalRotation(-90,0,0);
        this.floor.transform.setLocalPosition(0,-10,0);
        this.floor.transform.setLocalScale(100,100,1);
        this.context.renderer.addToCurrentScene(this.floor);

        console.log("current scene", this.context.renderer.getCurrentScene());

        this.y = 0;
    }

    update() {
        this.y += 0.01;
        this.floor.transform.setLocalPositionY( Math.sin(this.y)*10);
    }
}
