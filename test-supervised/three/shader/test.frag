uniform float frequency;
uniform float edgeWidth; // width of smooth step
varying float V; // generic varying
varying float lightIntensity;

void main()
{
    //float dp = length(vec2(dFdx(V), dFdy(V)));
    float dp = length(vec2(0.3,0.3));
    float logdp    = -log2(dp * 8.0);
    float ilogdp   = floor(logdp);
    float stripes  = exp2(ilogdp);

    float sawtooth = fract( (V) * frequency * stripes );
    float triangle = abs(2.0 * sawtooth - 1.0);

    // adjust line width
    float transition = logdp - ilogdp;

    // taper ends
    triangle = abs((1.0 + transition) * triangle - transition);

    float edge0  = clamp(lightIntensity - edgeWidth, 0.0, 1.0);
    float edge1  = clamp(lightIntensity, 0.0, 1.0);
    float square = 1.0 - smoothstep(edge0, edge1, triangle);

    gl_FragColor =  vec4(vec3(square), 1.0);
}
