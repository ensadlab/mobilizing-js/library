uniform vec3 scaleVector;

uniform vec3 lightPosition;

uniform vec3 diffuseMaterial;
//uniform vec3 ambientMaterial;
//uniform vec3 specularMaterial;
//uniform float shininess;

uniform vec3  hatchDirection;
uniform float lightness;

varying float V;
varying float lightIntensity;

void main(){

	vec4 vertex = vec4(position, 1.) * vec4(scaleVector, 1.);

	vec3 pos = vec3(modelViewMatrix * vertex);
    vec3 tnorm = normalMatrix * normal;
	vec3 lightVec = lightPosition;
    //vec3 lightVec =  vec3(0,0,1);

	//float grey = lightness * dot(vec4(.333,.333,.333,0), vec4(diffuseMaterial, 1.));
    float grey = lightness * dot(vec4(.333,.333,.333, 0), vec4(1, 1, 1, 1.));
	lightIntensity = max(grey * dot(lightVec, tnorm), 0.11);//last value for black

	V = dot( vec3(pos), hatchDirection);

	vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * modelViewPosition;

}
