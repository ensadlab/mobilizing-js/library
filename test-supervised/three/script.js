class Script {
    constructor() {

        this.needsUserInteraction = true;

        this.test = [
            "Erase",
            "Points",
            "Segments",
            "Svg",
            "CustomPlane",
            "ThreeRenderer",
            "Sphere",
            "IIWU",
            "CustomShader",
            "ObjLoading",
            "Line",
            "Arc",
            "Text",
            "TextOpentype",
            "Time",
            "GyroCam",
            "Clickable",
            "Texture",
            "VideoTexture",
            "RTT",
            "CubeMapRendering",
            "CubeMap",
            "Button",
            "ImageSequence",
            "Panel",
            "PanelStack",
            "SpaceNavigator",
            "DualShock4",
            "TextTroika",
        ]
    }

    preLoad(loader) {
        this.vertexShaderFile = loader.loadText({ "url": "./shader/test.vert" });
        this.fragmentShaderFile = loader.loadText({ "url": "./shader/test.frag" });

        this.sat = loader.loadText({ "url": "./model/SAT6.obj" });

        this.typefaceRequest = loader.loadJSON({ "url": "./fonts/DroidSans.json" });

        this.fontRequest = loader.loadArrayBuffer({ "url": "./fonts/Faune-DisplayBoldItalic.otf" });

        this.imageRequest = loader.loadImage({ "url": "./mire.jpg" });

        this.videoRequest = loader.loadVideo({ "url": "VilleVertebrale.m4v" });

        this.svgRequest = loader.loadText({ url: "./TimelineRecordPaint.svg" });

        this.billeRequests = [];

        for (let i = 0; i <= 25; i++) {
            const url = "./bille/test" + i + ".png";
            const request = loader.loadImage({ "url": url });
            this.billeRequests.push(request);
        }
    }

    setup() {
        this.renderer = new Mobilizing.three.RendererThree({
            // "antialias": true,
            //"preserveDrawingBuffer": true
        });
        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.renderer.addCamera(this.camera);

        //inputs
        this.touch = this.context.addComponent(new Mobilizing.input.Touch({ "target": this.renderer.canvas }));
        this.touch.setup();//set it up
        this.touch.on();//active it

        console.log(this.touch);

        this.mouse = this.context.addComponent(new Mobilizing.input.Mouse({ "target": this.renderer.canvas }));
        this.mouse.setup();//set it up
        this.mouse.on();//active it

        this.pointer = new Mobilizing.input.Pointer();
        this.context.addComponent(this.pointer);
        this.pointer.add(this.touch);
        this.pointer.add(this.mouse);

        this.buildMenu();
    }

    testThreeRenderer() {
        this.renderer.setClearColor(Mobilizing.three.Color.darkGray.clone());

        let light = new Mobilizing.three.Light();
        console.log(light);
        this.renderer.addToCurrentScene(light);

        let box1 = new Mobilizing.three.Box();
        box1.transform.setLocalPosition(0, 0, -10);
        box1.transform.setLocalRotation(33, 20, -33);
        this.renderer.addToCurrentScene(box1);
    }

    testErase() {

        let cube = new Mobilizing.three.Cube({ material: "basic" });
        this.renderer.addToCurrentScene(cube);

        const child1 = new Mobilizing.three.Cube({ material: "basic", name: "child1" });
        cube.transform.addChild(child1.transform);

        const child2 = new Mobilizing.three.Cube({ material: "basic", name: "child2" });
        cube.transform.addChild(child2.transform);

        const child11 = new Mobilizing.three.Cube({ material: "basic", name: "child11" });
        child1.transform.addChild(child11.transform);

        const child12 = new Mobilizing.three.Cube({ material: "basic", name: "child12" });
        child1.transform.addChild(child12.transform);

        console.log(cube.transform.getChildren(true));

        setTimeout(() => {
            console.log(this.renderer.getInfo());
        }, 100);

        setTimeout(() => {

            this.renderer.removeFromCurrentScene(cube);
            console.log(cube);
            cube.erase(true);
            cube = null;
            console.log(cube);
            console.log(this.renderer.getInfo());
        }, 2000);
    }

    testPoints() {

        const p = [];
        for (let i = 0; i < 64; i++) {
            p.push(new Mobilizing.three.Vector3(i, i % 2, 0));
        }

        const mesh = new Mobilizing.three.Points({
            "points": p,
        });
        mesh.setPointsSize(2);
        mesh.transform.setLocalPositionZ(-50);
        this.renderer.addToCurrentScene(mesh);
        console.log(mesh);

        const vertex = mesh.getVertices();
        for (let i = 0; i < vertex.length; i++) {
            const color = Mobilizing.three.Color.random();
            mesh.setPointColor(i, color);
        }

        this.pointer.events.on("on", (e) => {
            //console.log(e);

            const pointTotal = Mobilizing.math.randomFromTo(10, 100);
            const points = [];

            for (let i = 0; i < pointTotal; i++) {
                const vec = new Mobilizing.three.Vector3(
                    Mobilizing.math.randomFromTo(-10, 10),
                    Mobilizing.math.randomFromTo(-10, 10),
                    Mobilizing.math.randomFromTo(-10, 10)
                )
                points.push(vec);
            }
            console.log(points);
            mesh.setVertices(points);

            // points.forEach((point, index) => {
            //     mesh.setPoint(index, point);
            // });

        });
    }

    testSegments() {

        const p = [];
        for (let i = 0; i < 64; i++) {
            p.push(new Mobilizing.three.Vector3(i, i % 2, 0));
        }

        const mesh = new Mobilizing.three.Segments({
            "points": p,
        });
        mesh.transform.setLocalPositionZ(-50);
        this.renderer.addToCurrentScene(mesh);
        console.log(mesh);

        const vertex = mesh.getVertices();
        for (let i = 0; i < vertex.length; i++) {
            const color = Mobilizing.three.Color.random();
            mesh.setPointColor(i, color);
        }
    }

    testSvg() {

        const svgFile = this.svgRequest.getValue();
        console.log(this.svgRequest, svgFile);
        const mesh = Mobilizing.three.MeshImport.importFromFile({
            file: svgFile,
            url: this.svgRequest.url
        });
        console.log(mesh);
        mesh.material.setWireframe(true);
        mesh.transform.setLocalPositionZ(-100);
        this.renderer.addToCurrentScene(mesh);
    }

    testSphere() {
        let light = new Mobilizing.three.Light();
        console.log(light);
        this.renderer.addToCurrentScene(light);

        const sphereLambert = new Mobilizing.three.Sphere({ "material": "phong", "segments": 32 });
        sphereLambert.material.setShading("smooth");
        sphereLambert.transform.setLocalPosition(0, 0, -10);
        this.renderer.addToCurrentScene(sphereLambert);
    }

    testIIWU() {
        this.camera.setFOV(30);
        this.camera.setToPixel(); //adapt automatically the camera to the window

        let size = this.renderer.getCanvasSize();

        this.light = new Mobilizing.three.Light({ type: "spot" });
        this.light.transform.setLocalPosition(size.width / 2, -size.height / 2, 1000);
        this.light.setTargetPosition(size.width / 2, -size.height / 2, 0);
        this.light.setAngle(Math.PI / 30);
        this.light.setPenumbra(.5);
        this.light.setIntensity(1.5);
        this.renderer.addToCurrentScene(this.light);

        this.node = new Mobilizing.three.Node();
        this.node.transform.setLocalPosition(size.width / 2, -size.height / 2);
        this.renderer.addToCurrentScene(this.node);

        for (let i = 0; i < 200; i++) {
            let temp = new Mobilizing.three.Cube();
            temp.transform.setLocalPosition(Mobilizing.math.randomFromTo(-10, 10),
                Mobilizing.math.randomFromTo(-10, 10),
                Mobilizing.math.randomFromTo(-10, 10));
            temp.transform.setLocalScale(3);
            this.node.transform.addChild(temp.transform);
        }
        this.node.transform.setLocalScale(30);

        this.renderer.setFog("linear");
        this.renderer.setFogNear(this.node.getBoundingSphere().radius);
        this.renderer.setFogFar(Math.abs(this.camera.transform.getLocalPositionZ()) * 1.5);

    }

    testCustomShader() {
        this.renderer.setClearColor(Mobilizing.three.Color.gray.clone());

        this.camera.transform.setLocalPositionZ(10);

        this.y = 0;

        let vertexShader = this.vertexShaderFile.getValue();
        let fragmentShader = this.fragmentShaderFile.getValue();

        let light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(1, 1, 1);
        this.renderer.addToCurrentScene(light);

        /* let uniforms = {
            "mRefractionRatio": { value: 1.02 },
            "mFresnelBias": { value: 0.1 },
            "mFresnelPower": { value: 2.0 },
            "mFresnelScale": { value: 1.0 },
            "tCube": { value: null }
        } */

        let uniforms =
        {
            "hatchDirection": { value: new Mobilizing.three.Vector3(1, 1, 0) },
            "frequency": { value: 100 },
            "edgeWidth": { value: 0.1 },
            "lightness": { value: .10 },
            "scaleVector": { value: new Mobilizing.three.Vector3(1, 1, 1) },
            "lightPosition": { value: new Mobilizing.three.Vector3(-10, 10, 5) }
        }

        let material = new Mobilizing.three.Material({
            "type": "custom",
            "customParams": {
                "vertexShader": vertexShader,
                "fragmentShader": fragmentShader,
                "uniforms": uniforms
            }
        });
        material.setColor(Mobilizing.three.Color.white.clone());

        this.ico = new Mobilizing.three.Icosahedron({ "material": material });
        this.renderer.addToCurrentScene(this.ico);
        this.ico.transform.setLocalPosition(0, 0, 0);

        this.cube = new Mobilizing.three.Cube({ "material": material });
        this.renderer.addToCurrentScene(this.cube);
        this.cube.transform.setLocalPosition(-1, 1, 0);
    }

    testObjLoading() {
        this.camera.transform.setLocalPosition(-300, 200, 500);
        let objData = this.sat.getValue();
        console.log(objData);
        this.light = new Mobilizing.three.Light();
        this.renderer.addToCurrentScene(this.light);

        let obj = Mobilizing.three.MeshImport.importFromFile({
            file: objData,
            url: this.sat.url
        });
        console.log(obj);

        let edges = new Mobilizing.three.EdgesMesh({ "inputMesh": obj });

        this.renderer.addToCurrentScene(edges);

    }

    testLine() {
        this.camera.transform.setLocalPositionZ(100);

        this.line = new Mobilizing.three.Line({
            "point1": new Mobilizing.three.Vector3(-10, 0, 0), "point2": new Mobilizing.three.Vector3(10, 0, 0)
        });

        this.renderer.addToCurrentScene(this.line);

        this.toAdd1 = new Mobilizing.three.Vector3(0, .1, 0);
        this.toAdd2 = new Mobilizing.three.Vector3(0, -.1, 0);
    }

    testCustomPlane() {

        let light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(100, 100, 100);
        this.renderer.addToCurrentScene(light);

        this.camera.transform.setLocalPositionZ(50);

        this.planeMesh = new Mobilizing.three.Mesh({
            "material": "line"
        });
        this.planeMesh.material.setWireframe(true);

        const val = 5;
        const point1 = new Mobilizing.three.Vector3(-val, -val, 0);
        const point2 = new Mobilizing.three.Vector3(val, -val, 0);
        const point3 = new Mobilizing.three.Vector3(val, val, 0);
        const point4 = new Mobilizing.three.Vector3(-val, val, 0);

        this.planeMesh.pushQuad(point1, point2, point3, point4);

        this.renderer.addToCurrentScene(this.planeMesh);

        const isGeometryClockWise = Mobilizing.three.Mesh.geometryIsCW(this.planeMesh);
        console.log(isGeometryClockWise);

        //this.planeMesh.syncVerticesArrays("array");
        console.log(this.planeMesh);
    }

    testArc() {
        let light = new Mobilizing.three.Light();
        console.log(light);
        light.transform.setLocalPosition(100, 100, 100);
        this.renderer.addToCurrentScene(light);

        this.camera.transform.setLocalPositionZ(50);

        this.arc = new Mobilizing.three.Arc({
            "radius": 10,
            "endAngle": Math.PI / 1.5,
            "width": 5,
            //"material": "line"
        });

        this.renderer.addToCurrentScene(this.arc);
        //this.arc.material.setWireframe(true);

        console.log(this.arc);

        this.circle = new Mobilizing.three.Circle({
            "radius": 10,
            "endAngle": Math.PI / 1.5
        });

        this.renderer.addToCurrentScene(this.circle);
        this.circle.transform.setLocalPositionX(-20);
    }

    testText() {
        let typeface = this.typefaceRequest.getValue();

        let text2 = new Mobilizing.three.Text({
            text: "More or less segments",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        console.log(text2);
        text2.material.setWireframe(true);
        text2.transform.setLocalPosition(0, 0, -5);

        this.renderer.addToCurrentScene(text2);
    }

    testTextOpentype() {
        let typeface = this.typefaceRequest.getValue();
        let font = this.fontRequest.getValue();

        this.txt = new Mobilizing.three.TextOpentype({
            fontFile: font,
            text: "ceci est un text",
            depth: 0.1,
            material: "basic",
            //lineMode: true
        });
        /* this.txt = new Mobilizing.three.Text({
            primitive: "text",
            text: "ceci est un text",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 20,
            material: "basic",
            
        }); */
        console.log(this.txt);

        this.renderer.addToCurrentScene(this.txt);
        this.txt.material.setWireframe(true);
        this.txt.transform.setLocalPosition(-2, 0, -5);

        this.phrase = "";
        this.currentLetter = 0;
        this.newText = "une nouvelle phrase";

        this.pointer.events.on("on", (event) => {

            this.phrase += this.newText[this.currentLetter];
            this.currentLetter++;
            console.log(this.phrase);

            this.txt.setText(this.phrase);
        });
    }

    testTextTroika() {

        this.camera.transform.setLocalPositionZ(100);
        this.textTroika = new Mobilizing.three.TextTroika({
            fontFile: "fonts/Raleway-Bold-Italic.ttf",
            material: "basic",
            fontSize: 3,
        });
        this.textTroika.setColorForRange(0,5, Mobilizing.three.Color.green);
        this.textTroika.setColorForRange(2,4, Mobilizing.three.Color.red);
        this.renderer.addToCurrentScene(this.textTroika);

        setTimeout(() => {
            this.textTroika.setText("test de nouveau text");
            this.textTroika.material.setColor(Mobilizing.three.Color.red);
            this.textTroika.transform.setLocalRotationY(60);
        }, 2000);

        this.textTroika.events.on("update", (event) => {
            console.log(this.textTroika.getSize());
        });
    }

    testTime() {
        this.time = new Mobilizing.Time();
        this.time.scale = .1;
        this.time.setup();
        this.time.on();
        this.context.addComponent(this.time);

        console.log(this.time);
        this.cube = new Mobilizing.three.Cube({ material: "basic" });
        this.cube.transform.setLocalPosition(0, 0, -20);
        this.renderer.addToCurrentScene(this.cube);

        this.timeX = 0;
    }

    testGyroCam() {
        this.orientation = new Mobilizing.input.Orientation();
        this.context.addComponent(this.orientation);
        this.orientation.setup();
        this.orientation.on();

        this.gyroQuat = new Mobilizing.three.Quaternion();
        console.log(this.gyroQuat);

        let cube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });
        cube.transform.setLocalPosition(0, 0, 0);
        cube.transform.setLocalScale(10, 10, 10);
        cube.material.setWireframe(true);
        this.renderer.addToCurrentScene(cube);

    }

    testClickable() {
        this.camera.transform.setLocalPositionZ(10);

        let cube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });
        this.renderer.addToCurrentScene(cube);

        let clickable = new Mobilizing.three.Clickable({
            "mesh": cube,
            "canvas": this.renderer.canvas,
            "camera": this.camera,
            "onPress": () => { cube.material.setColor(Mobilizing.three.Color.red) },
            "onRelease": () => { cube.material.setColor(Mobilizing.three.Color.white) },
            "onEnter": () => { cube.material.setColor(Mobilizing.three.Color.gray) },
            "onLeave": () => { cube.material.setColor(Mobilizing.three.Color.white) }
        });
        this.context.addComponent(clickable);
        clickable.setup();
    }

    testTexture() {
        this.camera.transform.setLocalPositionZ(5);

        let image = this.imageRequest.getValue();

        let cube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });

        let texture = new Mobilizing.three.Texture({ "image": image });
        cube.material.setTexture(texture);

        this.renderer.addToCurrentScene(cube);

        cube.transform.setLocalRotation(33, 0, 33);
    }

    testVideoTexture() {
        this.camera.transform.setLocalPositionZ(100);

        let video = this.videoRequest.getValue();
        let texture = new Mobilizing.three.VideoTexture({ "video": video });
        texture.setup();
        texture.on();
        this.context.addComponent(texture);

        let cube = new Mobilizing.three.Box({
            "material": "basic",
            "width": texture.getWidth() / 10,
            "height": texture.getHeight() / 10,
            "depth": texture.getHeight() / 10
        });
        console.log(cube);
        cube.material.setTexture(texture);
        this.renderer.addToCurrentScene(cube);
        cube.transform.setLocalRotation(10, 40, 10);

        texture.play();
    }

    testRTT() {
        this.camera.setClearColor(Mobilizing.three.Color.gray.clone());
        this.camera.transform.setLocalPositionZ(50);

        let video = this.videoRequest.getValue();
        let texture = new Mobilizing.three.VideoTexture({ "video": video });
        texture.setup();
        this.context.addComponent(texture);

        let cube = new Mobilizing.three.Box({
            "material": "basic",
            "width": texture.getWidth() / 100,
            "height": texture.getHeight() / 100,
            "depth": texture.getHeight() / 100
        });

        cube.material.setTexture(texture);
        this.renderer.addToCurrentScene(cube);
        cube.transform.setLocalRotation(10, 40, 10);
        cube.transform.setLocalPositionX(-10);

        texture.on();

        let RTT = new Mobilizing.three.RenderTexture({
            "width": window.innerWidth,
            "height": window.innerHeight,
        });
        console.log(RTT);

        // const node = new Mobilizing.three.Node();
        // node

        let RTTcamera = new Mobilizing.three.Camera();
        RTTcamera.setClearColor(Mobilizing.three.Color.red.clone());
        //RTTcamera.setAutoClearColor(true);
        RTTcamera.setRenderTexture(RTT);
        RTTcamera.transform.setLocalPositionZ(10);
        RTTcamera.transform.setLocalRotation(10, 0, 0);
        RTTcamera.setLayer("rtt");
        this.renderer.addCamera(RTTcamera);

        let cubeRTT = new Mobilizing.three.Box({
            "material": "basic",
            "width": texture.getWidth() / 100,
            "height": texture.getHeight() / 100,
            "depth": texture.getHeight() / 100
        });

        cubeRTT.material.setTexture(texture);
        this.renderer.setCurrentScene("rtt");
        this.renderer.addToCurrentScene(cubeRTT);
        cubeRTT.transform.setLocalRotation(10, 40, 10);

        console.log(RTTcamera);

        let plane = new Mobilizing.three.Box({
            "material": "basic",
            "width": RTT.getWidth() / 100,
            "height": RTT.getHeight() / 100
        });

        plane.material.setTexture(RTT);
        this.renderer.setCurrentScene("default");
        this.renderer.addToCurrentScene(plane);

    }

    testCubeMap() {

        let typeface = this.typefaceRequest.getValue();

        const cubeMapCamera = new Mobilizing.three.CubeMapCamera({
            renderer: this.renderer,
            typeface,
            makeTestScene: true,
        });
        this.cubeCameraNode = cubeMapCamera.makeCubeCameraNode();
        console.log("cubeMapCamera", cubeMapCamera, this.cubeCameraNode);

        for (let i = 0; i < 50; i++) {
            const cube = new Mobilizing.three.Cube({
                material : "basic",
                size : 10
            });
            cube.material.setColor(Mobilizing.three.Color.random());
            cube.transform.setLocalPosition(
                Mobilizing.math.randomFromTo(-100, 100),
                Mobilizing.math.randomFromTo(-100, 100),
                Mobilizing.math.randomFromTo(-100, 100)
            )
            this.renderer.addToCurrentScene(cube);
        }
    }

    testCubeMapRendering() {

        this.camera.transform.setLocalPositionZ(30);
        //this.camera.transform.lookAt(new Mobilizing.three.Vector3(0, 10, 0));
        this.camera.setClearColor(Mobilizing.three.Color.gray.clone());

        //scene to render
        //this.renderer.setCurrentScene("cubemap");

        const scene = this.makeCubeCameraCheckScene(/* "cubemap" */);
        this.renderer.addToCurrentScene(scene);

        let image = this.imageRequest.getValue();

        let cube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });
        cube.material.setColor(Mobilizing.three.Color.gray);
        cube.transform.setLocalScale(30);
        let texture = new Mobilizing.three.Texture({ "image": image });
        cube.material.setTexture(texture);
        //this.renderer.addToCurrentScene(cube);

        this.cubeCameraNode = this.makeCubeCamera();
        this.renderer.addToCurrentScene(this.cubeCameraNode);
        //console.log("cubeCameraNode", cubeCameraNode);

        /* this.cubeCameraNode = this.makeCubeCamera({
            layerName: "cubemap",
            renderToTexture: true,
        });
        this.renderer.addToCurrentScene(this.cubeCameraNode);
        const cams = this.cubeCameraNode.transform.getChildren();
        const textures = [];
        cams.forEach((cam, index) => {

            const texture = cam.getRenderTexture();
            textures.push(texture);
            console.log("texture", texture);
            const plane = new Mobilizing.three.Plane({
                "material": "basic",
                "width": 5,
                "height": 5,
            });
            plane.transform.setLocalPositionX(index * 5);
            plane.transform.setLocalPositionY(5);
            plane.material.setTexture(texture);

            this.renderer.setCurrentScene("default");
            this.renderer.addToCurrentScene(plane);
        }); */
    }

    makeCubeCamera({
        layerName = null,
        renderToTexture = null
    } = {}) {

        const cubeCameraNode = new Mobilizing.three.Node();
        //this.renderer.addToCurrentScene(cubeCameraNode);

        const fov = 90; // negative fov is not an error
        const aspect = 1;
        const resolution = 512;

        const cameraRight = new Mobilizing.three.Camera({
            name: "cameraRight",
            fov,
            aspect
        });
        cameraRight.transform.setUpDirection(new Mobilizing.three.Vector3(0, 1, 0));
        cameraRight.transform.lookAt(new Mobilizing.three.Vector3(1, 0, 0));

        const cameraLeft = new Mobilizing.three.Camera({
            name: "cameraLeft",
            fov,
            aspect
        });
        cameraLeft.transform.setUpDirection(new Mobilizing.three.Vector3(0, 1, 0));
        cameraLeft.transform.lookAt(new Mobilizing.three.Vector3(- 1, 0, 0));

        const cameraTop = new Mobilizing.three.Camera({
            name: "cameraTop",
            fov,
            aspect
        });
        cameraTop.transform.setUpDirection(new Mobilizing.three.Vector3(0, 0, 1));
        cameraTop.transform.lookAt(new Mobilizing.three.Vector3(0, 1, 0));

        const cameraBottom = new Mobilizing.three.Camera({
            name: "cameraBottom",
            fov,
            aspect
        });
        cameraBottom.transform.setUpDirection(new Mobilizing.three.Vector3(0, 0, 1));
        cameraBottom.transform.lookAt(new Mobilizing.three.Vector3(0, - 1, 0));

        const cameraBack = new Mobilizing.three.Camera({
            name: "cameraBack",
            fov,
            aspect
        });
        cameraBack.transform.setUpDirection(new Mobilizing.three.Vector3(0, 1, 0));
        cameraBack.transform.lookAt(new Mobilizing.three.Vector3(0, 0, 1));

        const cameraFront = new Mobilizing.three.Camera({
            name: "cameraFront",
            fov,
            aspect
        });
        cameraFront.transform.setUpDirection(new Mobilizing.three.Vector3(0, 1, 0));
        cameraFront.transform.lookAt(new Mobilizing.three.Vector3(0, 0, - 1));

        // First line: right, back, left. - Second line: bottom, top, front.
        cubeCameraNode.transform.addChild(cameraBottom.transform);
        cubeCameraNode.transform.addChild(cameraTop.transform);
        cubeCameraNode.transform.addChild(cameraFront.transform);

        cubeCameraNode.transform.addChild(cameraRight.transform);
        cubeCameraNode.transform.addChild(cameraBack.transform);
        cubeCameraNode.transform.addChild(cameraLeft.transform);

        //RTT
        if (renderToTexture) {
            const cameraRightTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraRight.setRenderTexture(cameraRightTexture);
            const cameraLeftTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraLeft.setRenderTexture(cameraLeftTexture);
            const cameraTopTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraTop.setRenderTexture(cameraTopTexture);
            const cameraBottomTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraBottom.setRenderTexture(cameraBottomTexture);
            const cameraBackTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraBack.setRenderTexture(cameraBackTexture);
            const cameraFrontTexture = new Mobilizing.three.RenderTexture({
                "width": resolution,
                "height": resolution,
            });
            cameraFront.setRenderTexture(cameraFrontTexture);

            const cams = cubeCameraNode.transform.getChildren();
            cams.forEach((cam) => {
                cam.setLayer(layerName);
                const clearColor = Mobilizing.three.Color.random();
                cam.setClearColor(clearColor);
                if (layerName) {
                    this.renderer.setCurrentScene(layerName);
                }
                this.renderer.addCamera(cam);
                console.log(cam.transform.getLocalRotation());
            });
            this.renderer.setCurrentScene("default");
        }
        else {
            //compute viewport values
            const viewports = this.computeViewports(window.innerWidth, window.innerHeight);

            const cams = cubeCameraNode.transform.getChildren();
            cams.forEach((cam, index) => {
                const rect = viewports[index];
                console.log("cam :", cam.name, "rect", rect);
                cam.viewport = rect;
                const clearColor = Mobilizing.three.Color.random();
                cam.setClearColor(clearColor);
                this.renderer.addCamera(cam);
            });
        }
        return cubeCameraNode;
    }

    computeViewports(width, height) {
        const squareSize = Math.min(width / 3, height / 2);
        const squares = [];
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 3; j++) {
                const x = j * squareSize / width;
                const y = i * squareSize / height;
                const rect = new Mobilizing.three.Rect({
                    x: x,
                    y: y,
                    width: squareSize / width,
                    height: squareSize / height
                });
                squares.push(rect);
            }
        }
        return squares;
    }

    makeCubeCameraCheckScene(layerName) {

        const checkNode = new Mobilizing.three.Node();
        if (layerName) {
            this.renderer.setCurrentScene(layerName);
        }
        this.renderer.addToCurrentScene(checkNode);

        const typeface = this.typefaceRequest.getValue();
        //let font = this.fontRequest.getValue();

        const front = new Mobilizing.three.Text({
            text: "front",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(front.transform);

        const back = new Mobilizing.three.Text({
            text: "back",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(back.transform);

        const left = new Mobilizing.three.Text({
            text: "left",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(left.transform);

        const right = new Mobilizing.three.Text({
            text: "right",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(right.transform);

        const top = new Mobilizing.three.Text({
            text: "top",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(top.transform);

        const bottom = new Mobilizing.three.Text({
            text: "bottom",
            font: typeface,
            fontSize: 1,
            depth: 0,
            segments: 10,
            material: "basic"
        });
        checkNode.transform.addChild(bottom.transform);

        const children = checkNode.transform.getChildren();
        children.forEach((mesh) => {
            mesh.setCenter(0, 0, 0);
        });

        front.transform.setLocalPositionZ(-10);
        back.transform.setLocalPositionZ(10);
        back.transform.setLocalRotationY(-180);
        left.transform.setLocalPositionX(-10);
        left.transform.setLocalRotationY(90);
        right.transform.setLocalPositionX(10);
        right.transform.setLocalRotationY(-90);
        top.transform.setLocalPositionY(10);
        top.transform.setLocalRotationX(90);
        bottom.transform.setLocalPositionY(-10);
        bottom.transform.setLocalRotationY(180);
        bottom.transform.setLocalRotationX(90);

        return checkNode;
    }

    testButton() {
        this.camera.transform.setLocalPositionZ(50);

        /* const button = new Mobilizing.three.Button({
            camera: this.camera,
            pointer: this.pointer,
            width:4,
            height:1,
            strokeWidth: .1,
            text:"<b>CLICK ME...</b>",
            
            onPress: function(){
                console.log("you clicked me!", this);
            }
        });
        
        this.context.addComponent(button);
        button.setup();
        button.on();
        this.renderer.addToCurrentScene(button.root); */

        let light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(100, 100, 100);
        this.renderer.addToCurrentScene(light);

        let gridButtons = [];

        for (var i = 0; i < 6; i++) {
            let button = new Mobilizing.three.Button({
                camera: this.camera,
                pointer: this.pointer,
                //width:3,
                //height:1,
                radius: 1,
                strokeWidth: .1,
                text: "<b>CLICK ME...</b>",
                textSize: 20,

                onPress: function () {
                    console.log("you clicked me!", this);
                }
            });

            button.onPress = function () {
                console.log("clicked on", this);
            }

            this.context.addComponent(button);
            button.setup();

            gridButtons.push(button);
        }

        //organize them as a grid
        let grid1 = new Mobilizing.three.ButtonGroup({
            "buttons": gridButtons,
            "columns": 3,
            "mode": "honeycomb"
        });
        this.renderer.addToCurrentScene(grid1.root);
        //grid1.transform.setLocalPosition(0, 0, 0);
    }

    testImageSequence() {
        this.images = [];

        this.billeRequests.forEach((req) => {

            let texture = new Mobilizing.three.Texture({
                "image": req.getValue(),
            });
            this.images.push(texture);
        });

        this.imageSequence = new Mobilizing.three.ImageSequenceTexture({
            "textures": this.images,
            "sequenceMode": "backward",
            "lapsToDo": 3,
        });
        this.context.addComponent(this.imageSequence);
        this.imageSequence.setup();
        this.imageSequence.on();
        this.imageSequence.play();

        //this.imageSequence.setEnterFrame(10);
        //this.imageSequence.setOutFrame(20);

        this.billePlane = new Mobilizing.three.Plane({ "material": "basic" });
        this.renderer.addToCurrentScene(this.billePlane);

        this.camera.transform.setLocalPositionZ(10);

        this.billePlane.material.setTexture(this.imageSequence.getCurrentTexture());

        //autoupdate of the texture through events
        this.imageSequence.events.on("frameUpdate", () => {
            this.billePlane.material.setTexture(this.imageSequence.getCurrentTexture());
        });
    }

    testPanel() {
        let light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(1, 1, 100);
        this.renderer.addToCurrentScene(light);

        //------
        // Panel without title
        //------
        let panel = new Mobilizing.three.Panel({
            camera: this.camera,
            pointer: this.pointer,
            width: 30,
            height: 20,
            texture: new Mobilizing.three.Texture({ "image": this.imageRequest.getValue() })
        });

        this.context.addComponent(panel);

        panel.setup();
        panel.transform.setLocalPosition(-40, 0, 100);

        panel.events.on('click', function () {
            console.log("clicked!");
            this.setStrokeColor(Mobilizing.three.Color.random());
        }.bind(panel));

        this.renderer.addToCurrentScene(panel);

        //------
        // Panel with title
        //------
        let panelTitle = new Mobilizing.three.Panel({
            title: 'Panel title',
            camera: this.camera,
            pointer: this.pointer,
            width: 30,
            height: 40,
            texture: new Mobilizing.three.Texture({ "image": this.imageRequest.getValue() })
        });

        this.context.addComponent(panelTitle);

        panelTitle.setup();
        panelTitle.transform.setLocalPosition(0, 0, -100);

        panelTitle.events.on('click', function () {
            this.setStrokeColor(Mobilizing.three.Color.random());
        }.bind(panelTitle));

        this.renderer.addToCurrentScene(panelTitle);

        //------
        // Panel with custom style
        //------
        const fontBuffer = this.fontRequest.getValue();
        const font = new Mobilizing.Font({ fontFile: fontBuffer });

        console.log(font);
        let panelCustom = new Mobilizing.three.Panel({
            title: 'Customized panel',
            camera: this.camera,
            pointer: this.pointer,
            width: 30,
            height: 35,
            cutOff: 0,
            strokeWidth: 2,
            titleHeight: 5,
            strokeColor: Mobilizing.three.Color.red.clone(),
            font: font,
            texture: new Mobilizing.three.Texture({ "image": this.imageRequest.getValue() })
        });

        this.context.addComponent(panelCustom);

        panelCustom.setup();
        panelCustom.transform.setLocalPosition(40, 0, -100);

        panelCustom.events.on('click', function () {
            this.setStrokeColor(Mobilizing.three.Color.random());
        }.bind(panelCustom));

        this.renderer.addToCurrentScene(panelCustom);
    }

    testPanelStack() {

        let light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(1, 1, 100);
        this.renderer.addToCurrentScene(light);

        //------ 
        // Panel with title
        //------
        let panels = [];

        for (let i = 0; i < 13; i++) {

            let panelTitle = new Mobilizing.three.Panel({
                "title": "Panel title" + i,
                "camera": this.camera,
                "pointer": this.pointer,
                "width": 50,
                "height": 30,
                "texture": new Mobilizing.three.Texture({ "image": this.imageRequest.getValue() })
            });

            //panelTitle.transform.setLocalPositionZ(-100);

            this.context.addComponent(panelTitle);
            panelTitle.setup();

            panelTitle.events.on("click", function () {
                this.setStrokeColor(Mobilizing.three.Color.random());
                console.log("!!!you clicked", panelTitle.title, "!!!!");
            }.bind(panelTitle));

            panels.push(panelTitle);
        }

        const stack = new Mobilizing.three.PanelStack({
            "mode": "line",
            "panels": panels,
            "wheelRadius": 130,
            "wheelVerticalFactor": .5,
            "wheelDepthFactor": 1,
            "lineHorizontalOffset": 10,
            "lineDepthOffset": 2,
            "pointer": this.pointer,
            "camera": this.camera,
        });

        this.renderer.addToCurrentScene(stack);
        this.context.addComponent(stack);
        stack.setup();
        stack.on();

        stack.transform.setLocalPosition(0, 0, -200);
        //stack.transform.setLocalRotationY(20);

        this.touch.events.on("swipeup", () => {
            console.log("swipeup");
            stack.stepTo(-1);
        });
        this.touch.events.on("swipedown", () => {
            stack.stepTo(1);
        });

    }

    testSpaceNavigator() {

        this.spaceNavigator = new Mobilizing.three.SpaceNavigator({ movementAcceleration: 300 });
        this.context.addComponent(this.spaceNavigator);
        this.spaceNavigator.setup();
        this.spaceNavigator.on();

        this.spaceNavigator.events.on("connected", () => {
            console.log(this.renderer.getCanvas());
            this.renderer.getCanvas().requestPointerLock();
        });

        this.camNode = new Mobilizing.three.Node();
        this.renderer.addToCurrentScene(this.camNode);

        this.camera.transform.setLocalPositionZ(10);
        this.camNode.transform.addChild(this.camera.transform);

        this.square = new Mobilizing.three.Box({ "material": "basic" });
        this.renderer.addToCurrentScene(this.square);
    }

    testDualShock4() {

        this.square = new Mobilizing.three.Box({ "material": "basic" });
        this.square.material.setWireframe(true);
        this.renderer.addToCurrentScene(this.square);

        this.dualShock4 = new Mobilizing.three.DualShock4();
        this.context.addComponent(this.dualShock4);
        this.dualShock4.setup();
        this.dualShock4.on();

        this.camera.transform.setLocalPositionZ(10);

        this.dualShock4.events.on("leftPad", (event) => {
            //console.log("leftPad :", event);
        });

        this.dualShock4.events.on("rightPad", (padPos) => {
            //console.log("rightPad :", padPos);
        });

        this.dualShock4.events.on("square", (event) => {
            console.log("square :", event);
        });

        this.euler = new Mobilizing.three.Euler(0, 0, 0, 'YXZ');
        this.vector = this.camera.transform.getLocalPosition();
        this.PI_2 = Math.PI / 2;
        // Set to constrain the pitch of the camera
        // Range is 0 to Math.PI radians
        this.minPolarAngle = 0; // radians
        this.maxPolarAngle = Math.PI; // radians

    }

    update() {
        if (this.currentTest === "IIWU") {
            if (this.pointer.getState()) {
                this.light.transform.setLocalPosition(this.pointer.getX(), - this.pointer.getY());

                let lightPos = this.light.transform.getLocalPosition();
                this.light.setTargetPosition(lightPos.x, lightPos.y, 0);
            }

            this.node.transform.setLocalRotationY(this.node.transform.getLocalRotationY() + .1);
        }

        if (this.currentTest === "CustomShader") {
            this.y += 0.1;
            this.cube.transform.setLocalRotation(this.y, this.y * 2, this.y * 3);
            this.ico.transform.setLocalRotation(-this.y, -this.y * 2, -this.y * 3);
        }

        if (this.currentTest === "Line") {
            let points = this.line.getPoints();

            let point1 = points.point1;
            let point2 = points.point2;

            if (point1.y > 10 || point1.y < -10) {
                this.toAdd1.y = -this.toAdd1.y;
            }
            if (point2.y > 10 || point2.y < -10) {
                this.toAdd2.y = -this.toAdd2.y;
            }

            this.line.setPoints(point1.add(this.toAdd1), point2.add(this.toAdd2));
        }

        if (this.currentTest === "Time") {
            this.timeX += this.time.getDeltaSeconds();
            this.cube.transform.setLocalPositionX(this.timeX);
        }

        if (this.currentTest === "GyroCam") {
            this.gyroQuat.setFromGyro(this.orientation.compass);
            //console.log(this.gyroQuat);

            if (this.gyroQuat) {
                this.camera.transform.setLocalQuaternion(this.gyroQuat);
            }
        }

        if (this.currentTest === "CubeMapRendering" || this.currentTest === "CubeMap") {
            const rot = this.cubeCameraNode.transform.getLocalRotation();
            this.cubeCameraNode.transform.setLocalRotation(rot.x, rot.y + 1, rot.z);
        }

        if (this.currentTest === "SpaceNavigator") {
            const pos = this.spaceNavigator.getPosition(0.1);
            if (pos) {
                const baseZ = 10
                let newZ = baseZ + pos.y;
                if (newZ < 1) {
                    newZ = 1;
                }
                //console.log(newZ);
                this.camera.transform.setLocalPositionZ(newZ);
            }

            const rot = this.spaceNavigator.getRotation();
            if (rot) {
                const eulerY = new Mobilizing.three.Euler(0, rot.y, 0);
                const squareQuat = new Mobilizing.three.Quaternion().setFromEuler(eulerY);
                this.square.transform.setLocalQuaternion(squareQuat);

                const nodeEuler = new Mobilizing.three.Euler(rot.x, 0, 0);
                const nodeQuat = new Mobilizing.three.Quaternion().setFromEuler(nodeEuler);
                this.camNode.transform.setLocalQuaternion(nodeQuat);
            }
        }

        if (this.currentTest === "DualShock4") {
            //console.log(this.dualShock4);
            if (this.dualShock4.connected) {

                const buttonSpeed = 0.02;
                this.dualShock4.leftPadScaleFactor = 0.02;
                this.dualShock4.rightPadScaleFactor = 0.01;

                const rotPad = this.dualShock4.getRightPad();
                const movePad = this.dualShock4.getLeftPad();

                this.euler.setFromQuaternion(this.camera.transform.getLocalQuaternion());

                this.euler.y -= rotPad.x;
                this.euler.x -= rotPad.y;

                this.euler.x = Math.max(this.PI_2 - this.maxPolarAngle, Math.min(this.PI_2 - this.minPolarAngle, this.euler.x));

                const quat = new Mobilizing.three.Quaternion().setFromEuler(this.euler);
                this.camera.transform.setLocalQuaternion(quat);

                const directions = this.camera.transform.getDirections();
                this.vector.add(directions.forwardVector.multiplyScalar(movePad.y));
                this.vector.add(directions.leftVector.multiplyScalar(movePad.x));

                const top = this.dualShock4.getButton("top");
                if (top) {
                    this.vector.y += buttonSpeed;
                }

                const bottom = this.dualShock4.getButton("bottom");
                if (bottom) {
                    this.vector.y -= buttonSpeed;
                }

                this.camera.transform.setLocalPosition(this.vector);
            }

        }
    }


    //=======================
    buildMenu() {
        let menu = document.createElement("form");
        menu.style.left = "0px";
        menu.style.top = "0px";
        menu.style.position = "fixed";

        let select = document.createElement("select");
        select.name = "test";

        let option = document.createElement("option");
        option.innerHTML = "Choose your test";
        select.appendChild(option);

        menu.appendChild(select);

        for (let prop in this.test) {
            let option = document.createElement("option");
            option.innerHTML = this.test[prop];
            option.value = this.test[prop];
            select.appendChild(option);
        }

        document.body.appendChild(menu);

        menu.addEventListener("change", (event) => {

            let currentTest = event.srcElement.value;

            let testToCall = this["test" + currentTest];
            console.log("will test", currentTest);

            if (testToCall) {
                testToCall.call(this);
            }

            this.currentTest = currentTest;
        });
    }
}
