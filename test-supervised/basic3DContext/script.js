class UserScript
{
    constructor()
    {
        this.test = [
            "Cloth",
            "Chain",
            "SpringMass",
        ];
    }
    
    preLoad(loader)
    {
    }
    
    setup()
    {
        this.physicsEngine = new Mobilizing.PhysicsEngine();
        this.context.addComponent(this.physicsEngine);
        this.physicsEngine.setup();
        this.physicsEngine.on();
        
        //console.log(this, this.context);
        this.context.light.transform.setLocalPosition(100,50,500);
        this.context.camera.transform.setLocalPositionZ(200);
        
        this.buildMenu();
    }
    
    testChain()
    {
        this.context.camera.transform.setLocalPositionZ(100);
        
        this.physicsEngine.setGlobalGravity(new Mobilizing.three.Vector3(0,-1,0));
        this.physicsEngine.setGlobalWind(new Mobilizing.three.Vector3(0,1,0.01));
        
        //chain element total
        let count = 30;
        
        let bodies = [];
        
        //create all the objects
        for (let i=0; i<count; ++i)
        {
            let cube = new Mobilizing.three.Cube();
            this.context.renderer.addToCurrentScene(cube);
            
            cube.transform.setLocalPosition(0,2,0);
            cube.material.setColor(Mobilizing.three.Color.random());
            //addBody(position, mass, object)
            let body = this.physicsEngine.addBody(new Mobilizing.three.Vector3(0,2,0), 1, cube);
            bodies.push(body);
        }
        
        //make their joints, 2 fixed one and stick between all the others
        let jointf1 = this.physicsEngine.addFixedJoint(bodies[0], new Mobilizing.three.Vector3(-10,10,0));
        let jointf2 = this.physicsEngine.addFixedJoint(bodies[count-1], new Mobilizing.three.Vector3(10,10,0));
        
        let distance = 2;
        
        for (let i=0; i<count-1; ++i)
        {
            this.physicsEngine.addStickJoint(bodies[i], bodies[i+1], distance);
        }
    }
    
    testCloth()
    {
        this.cloth = [];
        this.clothX = 10;
        this.clothY = 10;
        
        this.physicsEngine.setGlobalGravity(new Mobilizing.three.Vector3(0,-1,0));
        this.physicsEngine.setGlobalWind(new Mobilizing.three.Vector3(0,1,0.01));
        
        for (let x=0; x<this.clothX; ++x)
        {
            for (let y=0; y<this.clothY; ++y)
            {
                let shape = new Mobilizing.three.Cube({
                    "radius" : 1,
                    //"segments" : 20
                });
                //shape.material.setShading("smooth");
                this.context.renderer.addToCurrentScene(shape);
                
                let body = this.physicsEngine.addBody(new Mobilizing.three.Vector3(x*2, y*2, 0), 1.01, shape);
                this.cloth.push(body);
            }
        }
        
        const clothDistance = 4;
        
        for (let x=0; x < this.clothX-1; ++x)
        {
            for (let y=0; y < this.clothY; ++y)
            {
                //horizontal sticks
                this.physicsEngine.addStringJoint(this.cloth[y*this.clothX+x], this.cloth[y*this.clothX+x+1], clothDistance);
            }
        }
        
        for (let x=0;x<this.clothX;++x)
        {
            for (let y=0;y<this.clothY-1;++y)
            {
                //vertical sticks
                this.physicsEngine.addStringJoint(this.cloth[y*this.clothX+x], this.cloth[(y+1) * this.clothX+x], clothDistance);
            }
        }
        this.physicsEngine.addFixedJoint(this.cloth[0], new Mobilizing.three.Vector3(-10,10,0));
        this.physicsEngine.addFixedJoint(this.cloth[this.clothX-1], new Mobilizing.three.Vector3(0,10,0));
        
        let timer = new Mobilizing.Timer({
            "interval" : 3000,
            "callback" : () => {
                console.log("wind!");
                this.physicsEngine.setGlobalWind(new Mobilizing.three.Vector3(0,1,Mobilizing.math.randomFromTo(-.01, .01)));
            }
        });
        this.context.addComponent(timer);
        timer.setup();
        timer.on();
        timer.start();
    }
    
    testSpringMass()
    {
        this.context.camera.transform.setLocalPositionZ(50);
        
        this.physicsEngine.setGlobalGravity(new Mobilizing.three.Vector3(0,-1,0))
        this.physicsEngine.setGlobalWind(new Mobilizing.three.Vector3(0,0,0));
        
        const spring_damping = 0.1;
        const spring_k = .01;
        const spring_len = 10;

        const cube = new Mobilizing.three.Cube();
        const bodyFix = this.physicsEngine.addBody(new Mobilizing.three.Vector3(0,10,0), 2, cube);
        this.context.renderer.addToCurrentScene(cube);

        const cube2 = new Mobilizing.three.Cube();
        cube2.material.setColor(Mobilizing.three.Color.red);
        const bodyMass = this.physicsEngine.addBody(new Mobilizing.three.Vector3(0,10,0), 2, cube2);
        this.context.renderer.addToCurrentScene(cube2);

        this.physicsEngine.addFixedJoint(bodyFix, new Mobilizing.three.Vector3(0,10,0));
        //addSpringJoint(body1, body2, distance, k, damping)
        this.physicsEngine.addSpringJoint(bodyFix, bodyMass, spring_len, spring_k, spring_damping);
    }
    
    update()
    {
    }
    
    buildMenu()
    {
        let menu = document.createElement("form");
        menu.style.left = "0px";
        menu.style.top = "0px";
        menu.style.position = "fixed";
        
        let select = document.createElement("select");
        select.name = "test";
        
        let option = document.createElement("option");
        option.innerHTML = "Choose your test";
        select.appendChild(option);
        
        menu.appendChild(select);
        
        for(let prop in this.test)
        {
            let option = document.createElement("option");
            option.innerHTML = this.test[prop];
            option.value = this.test[prop];
            select.appendChild(option);
        }
        
        document.body.appendChild(menu);
        
        menu.addEventListener("change", (event) => {
            
            let currentTest = event.srcElement.value;
            
            let testToCall = this["test" + currentTest];
            console.log("will test",currentTest);
            
            if(testToCall)
            {
                testToCall.call(this);
            }
            
            this.currentTest = currentTest;
        });
    }
}
