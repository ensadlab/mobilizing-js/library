class script {

    constructor() {

        this.needsUserInteraction = true;
        this.test = [

            "Font",
            "StyledLetter",
            "StyledTextElement",
            "RichText",
            "TextField",

            "Motion",
            "Mouse",
            "Touch",
            "Camera",
            "Gamepad",
            "Pointer",

            "Timer",

            "Audio",
            "AudioFromMedia",
            "OfflineAudio",

            "ThreeRenderer",
        ];

        //window.addEventListener("load", this.buildMenu.bind(this));
    }

    prepare() {
        this.context.events.on("prepareDone", () => {
            console.log("prepareDone");
        });
        console.log("on fait quelque chose avant le reste ici");
    }

    preLoad(loader) {
        console.log("preLoad");
        this.fontFile = loader.loadArrayBuffer({ url: "fonts/Raleway-Regular.ttf" });
        this.audioBufferRequest = loader.loadArrayBuffer({ url: "guitar1.wav" });
    }

    setup() {
        console.log(this, this.context);

        this.buildMenu();
    }

    testStyledLetter() {
        let fontBuffer = this.fontFile.getValue();
        console.log(fontBuffer);
        this.font = new Mobilizing.Font({ fontFile: fontBuffer });

        let styledLetter = new Mobilizing.StyledLetter({
            "letter": "W",
            "font": this.font,
        });
        console.log(styledLetter);
    }

    testStyledTextElement() {
        let fontBuffer = this.fontFile.getValue();
        console.log(fontBuffer);

        this.font = new Mobilizing.Font({ fontFile: fontBuffer });
        //@TODO passer en symétrie namespace avec la structure du repo.
        //this.font = new Mobilizing.text.Font( {fontFile: fontBuffer} );

        this.styledText = new Mobilizing.StyledTextElement({
            "text": "What the fuck!",
            "font": this.font,
        });
        console.log(this.styledText);
    }

    testRichText() {
        //beware of the very few html tags we support : b || strong, i || em, p, font with attributes color && size (face TODO).
        var text = "Ceci est <b><font face='Avenir' color='blue' size='10'>un text</font></b> qui <i>utilise</i> des <i><b>balises</b> HTML.</i><br>Mais <i>est-ce que <b>ça marche</b> bien <b>ou <strong>encore</strong></b> pas?</i><br>On dirait bien que <b><font color='red' size='22'>SIIII!</font></b>\nPour le moment, on utilise des fonts chargées par defaut, mais on peut passer en argument des fonts <b><font size='40' color='green'>preloadées</font></b>.\nNe pas faire n'importe quoi avec les balises reste de la <i><b>résponsabilité</i></b> de l'utilisateur. <p>Non, mais, faudrait quand même pas trop déconner non plus, <b><font size='30'>HEEEEIIIIIIN!</font></b></p><p><b><i>Normal quoi,</i> non ?</b></p>";

        //make a richText representation of html
        var richText = new Mobilizing.RichText({
            width: 512,
            height: 512,
            text: text,
            lineHeight: 1.5,
            margin: 50,
            backgroundColor: "transparent",
            textColor: "white",
            textAlign: "center"
        });

        let canvas = richText.getCanvas();

        document.body.appendChild(canvas);

        console.log("text getTextDimensions : ", richText.getTextDimensions());
    }

    testFont() {
        let fontBuffer = this.fontFile.getValue();
        console.log(fontBuffer);
        this.font = new Mobilizing.Font({ fontFile: fontBuffer });
        console.log(this.font);
        console.log(this.font.getTextSize("TRUC", 20));

        this.font64 = new Mobilizing.Font({ base64String: Mobilizing.Raleway.regular });
        console.log(this.font64);
        let textSize = this.font64.getTextSize("TRUC", 20);
        console.log(textSize);

        let canvas = document.createElement("canvas");
        canvas.width = textSize.width;
        canvas.height = textSize.height;

        this.font64.getFont().draw(canvas.getContext("2d"), "TRUC", 0, textSize.height, 20);

        document.body.appendChild(canvas);
    }

    testMotion() {
        console.log(this);
        this.motion = new Mobilizing.input.Motion();
        this.motion.setup();
        this.motion.on();
    }

    testMouse() {
        this.mouse = new Mobilizing.input.Mouse();
        this.mouse.setup();
        this.mouse.on();

        this.mouse.events.on("mousemove", (event) => {
            console.log("mousemove", event);
        });
    }

    testTouch() {
        this.touch = new Mobilizing.input.Touch();
        //must be added to context before setup
        this.context.addComponent(this.touch);
        this.touch.setup();
        this.touch.on();

        this.touch.events.on("touchmoved", (event) => {
            console.log("touchmoved", event);
        });

        this.touch.events.on("touchstart", (event) => {
            console.log("touchstart", event);
        });

        this.touch.events.on("swipeup", (event) => {
            console.log("swipeup", event);
        });
        this.touch.events.on("swipedown", (event) => {
            console.log("swipedown", event);
        });
        this.touch.events.on("swipeleft", (event) => {
            console.log("swipeleft", event);
        });
        this.touch.events.on("swiperight", (event) => {
            console.log("swiperight", event);
        });
    }

    testPointer() {
        //inputs
        this.touch = this.context.addComponent(new Mobilizing.input.Touch({ "target": window }));
        this.touch.setup();//set it up
        this.touch.on();//active it

        this.mouse = this.context.addComponent(new Mobilizing.input.Mouse({ "target": window }));
        this.mouse.setup();//set it up
        this.mouse.on();//active it

        this.pointer = new Mobilizing.input.Pointer();
        this.context.addComponent(this.pointer);
        this.pointer.add(this.touch);
        this.pointer.add(this.mouse);

        console.log(this.pointer);
    }

    testCamera() {
        const constraints = {
            audio: false,
            video: { facingMode: "user" }
        };
        this.camera = new Mobilizing.input.UserMedia({
            constraints: constraints,
            callback: this.cameraCallback.bind(this)
        });

        this.context.addComponent(this.camera);
        this.camera.setup();
        this.camera.on();
    }

    testGamepad() {
        this.gamepad = new Mobilizing.input.Gamepad();
        this.context.addComponent(this.gamepad);
        this.gamepad.setup();
        this.gamepad.on();
    }

    testTimer() {
        this.timer = new Mobilizing.Timer({
            interval: 1000,
            callback:
                () => {
                    console.log("timer", this);
                }
        });
        this.context.addComponent(this.timer);
        this.timer.setup();
        this.timer.on();
        this.timer.start();
    }

    testAudio() {

        this.mouse = new Mobilizing.input.Mouse({ target: window });
        this.mouse.setup();
        this.mouse.on();

        this.keyboard = new Mobilizing.input.Keyboard();
        this.keyboard.setup();
        this.keyboard.on();

        if (!this.audioRenderer) {

            this.audioRenderer = new Mobilizing.audio.Renderer();
            this.context.addComponent(this.audioRenderer);

            this.audioRenderer.on();
            console.log("beep!");
            this.audioRenderer.beep();

            this.source = new Mobilizing.audio.Source({
                renderer: this.audioRenderer,
                is3D: true,
                loop: true,
            });
            this.source.setup();
            this.source.on();
            this.context.addComponent(this.source);

            this.source.events.on("ended", (loopCount) =>{
                console.log("loopCount",loopCount);
            });

            const sound = new Mobilizing.audio.Buffer({
                renderer: this.audioRenderer,
                arrayBuffer: this.audioBufferRequest.getValue(),
                decodedCallback: () => {
                    console.log("play!");
                    this.source.setBuffer(sound);
                    this.source.play();
                }
            });
        }

        //keyboard interaction
        this.keyboard.events.on("keydown", (event) => {
            //console.log(event);
            let playbackRate = this.source.getPlaybackRate();

            if (event.key === "ArrowUp" || event.code === "ArrowUp") {
                console.log("ArrowUp");
                this.source.setPlaybackRate(playbackRate += 0.1);
            }
            else if (event.key === "ArrowDown" || event.code === "ArrowDown") {
                console.log("ArrowDown");
                this.source.setPlaybackRate(playbackRate -= 0.1);
            }
            console.log("playbackRate", playbackRate);
        });
    }

    testAudioFromMedia() {

        this.mouse = new Mobilizing.input.Mouse({ target: window });
        this.mouse.setup();
        this.mouse.on();

        let audioMedia = document.createElement("audio");
        audioMedia.src = "M83.mp3";

        this.mouse.events.on("mouseclick", (event) => {

            if (!this.audioRenderer) {

                this.audioRenderer = new Mobilizing.audio.Renderer();
                this.context.addComponent(this.audioRenderer);
                this.audioRenderer.on();

                let source = new Mobilizing.audio.Source({
                    renderer: this.audioRenderer,
                    mediaElement: audioMedia,
                    is3D: true
                });
                source.setLoop(true);
                source.setup();
                source.on();
                this.context.addComponent(source);

                source.play();

                console.log(source);
                window.source = source;
            }
        });
    }

    testOfflineAudio() {

        this.mouse = new Mobilizing.input.Mouse({ target: window });
        this.mouse.setup();
        this.mouse.on();

        this.mouse.events.on("mouseclick", (event) => {

            if (!this.audioRenderer) {

                this.audioRenderer = new Mobilizing.audio.OfflineRenderer();

                console.log("beep!");
                this.audioRenderer.beep();

                let source = new Mobilizing.audio.Source({
                    renderer: this.audioRenderer,
                    is3D: true
                });
                source.setLoop(true);
                source.setup();
                source.on();
                this.context.addComponent(source);

                let sound = new Mobilizing.audio.Buffer({
                    renderer: this.audioRenderer,
                    arrayBuffer: this.audioBufferRequest.getValue(),
                    decodedCallback: () => {
                        //console.log("play!");
                        source.setBuffer(sound);
                        source.setScheduleStartTime(1);
                        source.play();

                        this.audioRenderer.startRendering();
                    }
                });

                this.audioRenderer.events.on("complete", (buffer) => {

                    console.log("complete", buffer);

                    Mobilizing.audio.BufferToWav.downloadToWavFile(buffer, "sound");

                });

                window.source = source;
            }
        });
    }

    testTextField() {
        this.textField = new Mobilizing.TextField();
        document.body.appendChild(this.textField.getCanvas());

        let keyboard = new Mobilizing.input.Keyboard({ target: window });
        keyboard.setup();
        keyboard.on();
        keyboard.events.on("keydown", this.addNewLetter.bind(this));

        this.mouse = new Mobilizing.input.Mouse({ "target": this.textField.getCanvas() });
        this.mouse.setup();
        this.mouse.on();

        this.mouse.events.on("mousepress", (event, x, y) => {

            console.log("mousepress", x, y);
            let letterPicked = this.textField.pickLetter(x, y);

            if (letterPicked) {
                this.textField.moveCursorTo(letterPicked.index);
            }

        });
    }

    addNewLetter(event) {
        // See https://developer.mozilla.org/fr/docs/Web/API/KeyboardEvent/key/Key_Values
        if (event.key.length === 1) {
            console.log("addLetter", event.key);
            this.textField.addLetter(event.key);
        }
        else {
            console.log(event);
            switch (event.key) {
                case 'ArrowLeft':
                    this.textField.moveCursorBack();
                    break;

                case 'ArrowRight':
                    this.textField.moveCursorForward();
                    break;

                case 'Enter':
                    console.log("isEnter");
                    this.textField.addLetter("\n");
                    break;

                case 'Backspace':
                    console.log("isDelete");
                    this.textField.delete();
                    break;

                case 'Shift':
                    console.log("isShift");
                    this.textField.clear();
                    break;
            }
        }

        console.log(this.textField.getText());
    }

    testThreeRenderer() {
        console.log("build renderer");
        this.renderer = new Mobilizing.three.RendererThree();
        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.renderer.addCamera(this.camera);

        this.renderer.setClearColor(Mobilizing.three.Color.darkGray.clone());

        let light = new Mobilizing.three.Light();
        console.log(light);
        this.renderer.addToCurrentScene(light);

        let box1 = new Mobilizing.three.Box();
        box1.transform.setLocalPosition(0, 0, -10);
        box1.transform.setLocalRotation(33, 20, -33);
        this.renderer.addToCurrentScene(box1);
    }

    cameraCallback(userMedia) {

        this.cameraUserMedia = userMedia;
        this.canvas = userMedia.canvas;
        document.body.appendChild(this.canvas);

        this.cameraIsReady = true;

        console.log("cameraCallback", this);
    }

    update() {
        if (this.currentTest === "Motion") {
            console.log(this.motion.acc);
        }

        if (this.currentTest === "Camera") {
            if (this.cameraIsReady) {
                let drawContext = this.canvas.getContext("2d");
                drawContext.drawImage(this.cameraUserMedia.videoEl, 0, 0);

            }
        }

        if (this.currentTest === "Gamepad") {
            console.log(this.gamepad.getButton(0));
            console.log(this.gamepad.getAllAxes());
            console.log(this.gamepad.getAxes(0), this.gamepad.getAxes(1));
        }

        if (this.currentTest === "Pointer") {

            if (this.pointer.getState()) {

                console.log(this.pointer.pointers,
                    this.pointer.getX(0),
                    this.pointer.getY(0));
            }
        }
    }

    buildMenu() {
        let menu = document.createElement("form");
        menu.style.position = "fixed";

        let select = document.createElement("select");
        select.name = "test";

        let option = document.createElement("option");
        option.innerHTML = "Choose your test";
        select.appendChild(option);

        menu.appendChild(select);

        for (let prop in this.test) {
            let option = document.createElement("option");
            option.innerHTML = this.test[prop];
            option.value = this.test[prop];
            select.appendChild(option);
        }

        document.body.appendChild(menu);

        menu.addEventListener("change", (event) => {

            this.currentTest = event.srcElement.value;

            let testToCall = this["test" + this.currentTest];
            console.log("will test", this.currentTest);

            if (testToCall) {
                testToCall.call(this);
            }

        });
    }
}
